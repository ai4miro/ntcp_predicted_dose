#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 13:08:37 2020

@author: margerie huet dastarac
"""
# These functions calculate the NTCP as according to the models used by the Dutch 'Landelijk Indicatie Protocol Protonentherapie (LIPPv2.2)'


from math import isnan
import numpy as np


########### DEFINITIVE ################
modelparams_definitive_xero_grade2={"beta0": -2.2951, "beta_Parotid":0.0996, "beta_Subm":0.0182, "beta_baseline": [0,0.495,1.207]}
modelparams_definitive_xero_grade3={"beta0": -3.7286, "beta_Parotid":0.0855, "beta_Subm":0.0156, "beta_baseline": [0,0.4249,1.0361]}

modelparams_definitive_dysf_grade2={"beta0": -4.0536, "beta_Coral":0.03, "beta_PCMSup":0.0236, "beta_PCMMed":0.0095, "beta_PCMInf":0.0133, "beta_baseline": [0,0.9382,1.29], "beta_location":[0,-0.6281, -0.7711]}
modelparams_definitive_dysf_grade3={"beta0": -7.6174, "beta_Coral":0.0259, "beta_PCMSup":0.0203, "beta_PCMMed":0.0303, "beta_PCMInf":0.0341, "beta_baseline": [0,0.5738,1.4718], "beta_location":[0,0.0387, -0.5303]}

########### POSTOPERATIVE ################

modelparams_postop_xero_grade2={"beta0": -1.6824, "beta_Parotid":0.0388, "beta_Subm":0.0071, "beta_baseline": [0,0.1925,0.4695]}
modelparams_postop_xero_grade3={"beta0": -4.3613, "beta_Parotid":0.1054, "beta_Subm":0.0193, "beta_baseline": [0,0.5234,1.2763]}

modelparams_postop_dysf_grade2={"beta0": -2.4138, "beta_Coral":0.0192, "beta_PCMSup":0.0151, "beta_PCMMed":0.0060, "beta_PCMInf":0.0085, "beta_baseline": [0,0.5985,0.8227], "beta_location":[0,-0.4007, -0.4918]}
modelparams_postop_dysf_grade3={"beta0": -3.2594, "beta_Coral":0.0063, "beta_PCMSup":0.0050, "beta_PCMMed":0.0074, "beta_PCMInf":0.0084, "beta_baseline": [0,0.1404,0.3603], "beta_location":[0,0.0095, -0.1298]}


############ DEFINITIVE ############

# baseline_score: 0 not at all (EORTC QLQ-H&N35 Q41 score 1), 1 a bit (score 2), 2 moderate-severe (score 3-4)
def NTCP_definitive_xero_grade2(baseline_score, Dmean_ParL, Dmean_ParR, Dmean_SubmL, Dmean_SubmR, Volume_SubmL, Volume_SubmR):
    beta={}
    beta["Constant"] = modelparams_definitive_xero_grade2["beta0"]+modelparams_definitive_xero_grade2["beta_baseline"][baseline_score]
    beta["Parotid"] = modelparams_definitive_xero_grade2["beta_Parotid"]
    beta["Subm"] = modelparams_definitive_xero_grade2["beta_Subm"]
    Dmean_ParL = 0 if isnan(Dmean_ParL) else Dmean_ParL
    Dmean_ParR = 0 if isnan(Dmean_ParR) else Dmean_ParR
    Dmean_SubmL = 0 if isnan(Dmean_SubmL) else Dmean_SubmL
    Dmean_SubmR = 0 if isnan(Dmean_SubmR) else Dmean_SubmR
    #print("Xero2","Dmean_ParL",Dmean_ParL,"Dmean_ParR",Dmean_ParR,"Dmean_SubmL",Dmean_SubmL,"Dmean_SubmR",Dmean_SubmR)
    Dmean_SubmComb=(Dmean_SubmL*Volume_SubmL+Dmean_SubmR*Volume_SubmR)/(Volume_SubmL+Volume_SubmR)
    Dmean_ParComb=np.sqrt(Dmean_ParL) + np.sqrt(Dmean_ParR)
    
    S=beta["Constant"]+beta["Parotid"]*Dmean_ParComb+beta["Subm"]*Dmean_SubmComb
    
    NTCP=1/(1+np.exp(-S))
    print("NTCP xero 2", NTCP)
    return NTCP

# baseline_score: 0 not at all (EORTC QLQ-H&N35 Q41 score 1), 1 a bit (score 2), 2 moderate-severe (score 3-4)
def NTCP_definitive_xero_grade3(baseline_score, Dmean_ParL, Dmean_ParR, Dmean_SubmL, Dmean_SubmR, Volume_SubmL, Volume_SubmR):
    beta={}
    beta["Constant"] = modelparams_definitive_xero_grade3["beta0"]+modelparams_definitive_xero_grade3["beta_baseline"][baseline_score]
    beta["Parotid"] = modelparams_definitive_xero_grade3["beta_Parotid"]
    beta["Subm"] = modelparams_definitive_xero_grade3["beta_Subm"]
    Dmean_ParL = 0 if isnan(Dmean_ParL) else Dmean_ParL
    Dmean_ParR = 0 if isnan(Dmean_ParR) else Dmean_ParR
    Dmean_SubmL = 0 if isnan(Dmean_SubmL) else Dmean_SubmL
    Dmean_SubmR = 0 if isnan(Dmean_SubmR) else Dmean_SubmR
    # print("Xero3","Dmean_ParL",Dmean_ParL,"Dmean_ParR",Dmean_ParR,"Dmean_SubmL",Dmean_SubmL,"Dmean_SubmR",Dmean_SubmR)
    
    Dmean_SubmComb=(Dmean_SubmL*Volume_SubmL+Dmean_SubmR*Volume_SubmR)/(Volume_SubmL+Volume_SubmR)
    Dmean_ParComb=np.sqrt(Dmean_ParL) + np.sqrt(Dmean_ParR)
    
    S=beta["Constant"]+beta["Parotid"]*Dmean_ParComb+beta["Subm"]*Dmean_SubmComb
    
    NTCP=1/(1+np.exp(-S))
    print("NTCP xero 3", NTCP)
    return NTCP

# baseline_score: 0 grade 0-I, 1 grade II, 2 grade III-IV
# location: 0 oral cavity, 1 farynx, 2 larynx
def NTCP_definitive_dysf_grade2(baseline_score, Dmean_Coral, Dmean_PCMSup, Dmean_PCMMed, Dmean_PCMInf, location):
    beta={}
    beta["Constant"]=modelparams_definitive_dysf_grade2["beta0"]+modelparams_definitive_dysf_grade2["beta_baseline"][baseline_score]+modelparams_definitive_dysf_grade2["beta_location"][location]
    beta["Coral"]=modelparams_definitive_dysf_grade2["beta_Coral"]
    beta["PCMSup"]=modelparams_definitive_dysf_grade2["beta_PCMSup"]
    beta["PCMMed"]=modelparams_definitive_dysf_grade2["beta_PCMMed"]
    beta["PCMInf"]=modelparams_definitive_dysf_grade2["beta_PCMInf"]
        
    S=beta["Constant"]+beta["Coral"]*Dmean_Coral+beta["PCMSup"]*Dmean_PCMSup+beta["PCMMed"]*Dmean_PCMMed+beta["PCMInf"]*Dmean_PCMInf
    # print('S',S)
    NTCP=1/(1+np.exp(-S))
    # print("NTCP",NTCP)
    return NTCP

# baseline_score: 0 grade 0-I, 1 grade II, 2 grade III-IV
# location: 0 oral cavity, 1 farynx, 2 larynx
def NTCP_definitive_dysf_grade3(baseline_score, Dmean_Coral, Dmean_PCMSup, Dmean_PCMMed, Dmean_PCMInf, location):
    beta={}
    beta["Constant"]=modelparams_definitive_dysf_grade3["beta0"]+modelparams_definitive_dysf_grade3["beta_baseline"][baseline_score]+modelparams_definitive_dysf_grade3["beta_location"][location]
    beta["Coral"]=modelparams_definitive_dysf_grade3["beta_Coral"]
    beta["PCMSup"]=modelparams_definitive_dysf_grade3["beta_PCMSup"]
    beta["PCMMed"]=modelparams_definitive_dysf_grade3["beta_PCMMed"]
    beta["PCMInf"]=modelparams_definitive_dysf_grade3["beta_PCMInf"]
        
    S=beta["Constant"]+beta["Coral"]*Dmean_Coral+beta["PCMSup"]*Dmean_PCMSup+beta["PCMMed"]*Dmean_PCMMed+beta["PCMInf"]*Dmean_PCMInf
    
    NTCP=1/(1+np.exp(-S))
    
    return NTCP

###########@ POSTOPERATIVE ############


# baseline_score: 0 not at all (EORTC QLQ-H&N35 Q41 score 1), 1 a bit (score 2), 2 moderate-severe (score 3-4)
def NTCP_postop_xero_grade2(baseline_score, Dmean_ParL, Dmean_ParR, Dmean_SubmL, Dmean_SubmR, Volume_SubmL, Volume_SubmR):
    beta={}
    beta["Constant"] = modelparams_postop_xero_grade2["beta0"]+modelparams_postop_xero_grade2["beta_baseline"][baseline_score]
    beta["Parotid"] = modelparams_postop_xero_grade2["beta_Parotid"]
    beta["Subm"] = modelparams_postop_xero_grade2["beta_Subm"]

    Dmean_SubmComb=(Dmean_SubmL*Volume_SubmL+Dmean_SubmR*Volume_SubmR)/(Volume_SubmL+Volume_SubmR)
    Dmean_ParComb=np.sqrt(Dmean_ParL) + np.sqrt(Dmean_ParR)
    
    S=beta["Constant"]+beta["Parotid"]*Dmean_ParComb+beta["Subm"]*Dmean_SubmComb
    
    NTCP=1/(1+np.exp(-S))
    
    return NTCP

# baseline_score: 0 not at all (EORTC QLQ-H&N35 Q41 score 1), 1 a bit (score 2), 2 moderate-severe (score 3-4)
def NTCP_postop_xero_grade3(baseline_score, Dmean_ParL, Dmean_ParR, Dmean_SubmL, Dmean_SubmR, Volume_SubmL, Volume_SubmR):
    beta={}
    beta["Constant"] = modelparams_postop_xero_grade3["beta0"]+modelparams_postop_xero_grade3["beta_baseline"][baseline_score]
    beta["Parotid"] = modelparams_postop_xero_grade3["beta_Parotid"]
    beta["Subm"] = modelparams_postop_xero_grade3["beta_Subm"]

    Dmean_SubmComb=(Dmean_SubmL*Volume_SubmL+Dmean_SubmR*Volume_SubmR)/(Volume_SubmL+Volume_SubmR)
    Dmean_ParComb=np.sqrt(Dmean_ParL) + np.sqrt(Dmean_ParR)
    
    S=beta["Constant"]+beta["Parotid"]*Dmean_ParComb+beta["Subm"]*Dmean_SubmComb
    
    NTCP=1/(1+np.exp(-S))
    
    return NTCP

# baseline_score: 0 grade 0-I, 1 grade II, 2 grade III-IV
# location: 0 oral cavity, 1 farynx, 2 larynx
def NTCP_postop_dysf_grade2(baseline_score, Dmean_Coral, Dmean_PCMSup, Dmean_PCMMed, Dmean_PCMInf, location):
    beta={}
    beta["Constant"]=modelparams_postop_dysf_grade2["beta0"]+modelparams_postop_dysf_grade2["beta_baseline"][baseline_score]+modelparams_postop_dysf_grade2["beta_location"][location]
    beta["Coral"]=modelparams_postop_dysf_grade2["beta_Coral"]
    beta["PCMSup"]=modelparams_postop_dysf_grade2["beta_PCMSup"]
    beta["PCMMed"]=modelparams_postop_dysf_grade2["beta_PCMMed"]
    beta["PCMInf"]=modelparams_postop_dysf_grade2["beta_PCMInf"]
        
    S=beta["Constant"]+beta["Coral"]*Dmean_Coral+beta["PCMSup"]*Dmean_PCMSup+beta["PCMMed"]*Dmean_PCMMed+beta["PCMInf"]*Dmean_PCMInf
    
    NTCP=1/(1+np.exp(-S))
    
    return NTCP

# baseline_score: 0 grade 0-I, 1 grade II, 2 grade III-IV
# location: 0 oral cavity, 1 farynx, 2 larynx
def NTCP_postop_dysf_grade3(baseline_score, Dmean_Coral, Dmean_PCMSup, Dmean_PCMMed, Dmean_PCMInf, location):
    beta={}
    beta["Constant"]=modelparams_postop_dysf_grade3["beta0"]+modelparams_postop_dysf_grade3["beta_baseline"][baseline_score]+modelparams_postop_dysf_grade3["beta_location"][location]
    beta["Coral"]=modelparams_postop_dysf_grade3["beta_Coral"]
    beta["PCMSup"]=modelparams_postop_dysf_grade3["beta_PCMSup"]
    beta["PCMMed"]=modelparams_postop_dysf_grade3["beta_PCMMed"]
    beta["PCMInf"]=modelparams_postop_dysf_grade3["beta_PCMInf"]
        
    S=beta["Constant"]+beta["Coral"]*Dmean_Coral+beta["PCMSup"]*Dmean_PCMSup+beta["PCMMed"]*Dmean_PCMMed+beta["PCMInf"]*Dmean_PCMInf
    
    NTCP=1/(1+np.exp(-S))
    
    return NTCP


# def ntcp_LIPPv2(photon_dose,proton_dose,struct):