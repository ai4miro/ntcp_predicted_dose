import numpy as np 
import nibabel as nib
import os, sys
from utils import *
from HAN_NTCP import *
from losses import mse_final
import matplotlib.pyplot as plt

patients_pt_gt=r"/path/to/nii/PT/data/" #groundtruth
patients_vmat_gt=r"/path/to/nii/VMAT/data/" #groundtruth
predfolder_pt = r'/path/to/folder/containing/pt/dose/predictions' 
predfolder_vmat = r'/path/to/folder/containing/vmat/dose/predictions' 
figure_folder = r'/path/to/folder/where/figures/are/stored'
struct_labels = ['BODY', 'Brainstem', 'Esophagus_upper', 'GlotticArea', 'OralCavity', 'Parotid_L', 'Parotid_R', 'PharConsInf', 'PharConsMid', 'PharConsSup', 'SpinalCord', 'Submandibular_L', 'Submandibular_R', 'SupraglotLarynx']
innerfile = "inner_file_name"
# Parameters of the network
params = dict(learn_rate =  3e-4,
            loss_function =  mse_final,
            number_of_pool = 4,
            img_rows =       96, 
            img_cols =       96,
            img_slcs =       64,
            dense_enc_dec =  True,
            instance_norm =  True,
            dilate_bottleneck = False
            )
result_folder_pt = os.path.join(predfolder_pt,params2name(params),'test_predictions','ntcp_norm_baseline0')
result_folder_vmat = os.path.join(predfolder_vmat,params2name(params),'test_predictions','ntcp_norm_baseline0')
result_folder_pt_gt = os.path.join(predfolder_pt,params2name(params),'test_predictions','gt_ntcp_norm_baseline0')
result_folder_vmat_gt = os.path.join(predfolder_vmat,params2name(params),'test_predictions','gt_ntcp_norm_baseline0')
result_folder_pt_d0 = os.path.join(predfolder_pt,params2name(params),'test_predictions','d0_ntcp_norm_baseline0')
result_folder_vmat_d0 = os.path.join(predfolder_vmat,params2name(params),'test_predictions','d0_ntcp_norm_baseline0')

if not os.path.exists(result_folder_pt):
    os.makedirs(result_folder_pt)
if not os.path.exists(result_folder_vmat):
    os.makedirs(result_folder_vmat)
if not os.path.exists(result_folder_pt_gt):
    os.makedirs(result_folder_pt_gt)
if not os.path.exists(result_folder_vmat_gt):
    os.makedirs(result_folder_vmat_gt)
if not os.path.exists(result_folder_pt_d0):
    os.makedirs(result_folder_pt_d0)
if not os.path.exists(result_folder_vmat_d0):
    os.makedirs(result_folder_vmat_d0)
patients_pt = os.path.join(predfolder_pt, params2name(params),'test_predictions','pred_nii_norm_ctvp')
patients_vmat = os.path.join(predfolder_vmat, params2name(params),'test_predictions','pred_nii_norm_ctvp')
baseline_score = 0 # moderate to serious (matige-tot-ernstige)
xt_ref=0
pt_ref=0
nb_crossval = 11

def ntcp_xero_2(dose,struct_split):
    # PT
    metrics=['Dmean']
    num_bins = 800
    dvh_ParL, bins = get_DVH(dose,struct_split[...,5],num_bins)
    Dmean_ParL = get_DVH_metrics(metrics,dvh_ParL,bins,dose,struct_split[...,5])[0]
    dvh_ParR, bins = get_DVH(dose,struct_split[...,6],num_bins)
    Dmean_ParR = get_DVH_metrics(metrics,dvh_ParR,bins,dose,struct_split[...,6])[0]
    dvh_SubmL, bins = get_DVH(dose,struct_split[...,11],num_bins)
    Dmean_SubmL = get_DVH_metrics(metrics,dvh_SubmL,bins,dose,struct_split[...,11])[0]
    dvh_SubmR, bins = get_DVH(dose,struct_split[...,12],num_bins)
    Dmean_SubmR = get_DVH_metrics(metrics,dvh_SubmR,bins,dose,struct_split[...,12])[0]
    Volume_SubmL = np.sum(struct_split[...,11])
    Volume_SubmR = np.sum(struct_split[...,12])
    return  NTCP_definitive_xero_grade2(baseline_score, Dmean_ParL, Dmean_ParR, Dmean_SubmL, Dmean_SubmR, Volume_SubmL, Volume_SubmR)
    
def ntcp_xero_3(dose,struct_split):
    # PT
    metrics=['Dmean']
    num_bins = 800
    dvh_ParL, bins = get_DVH(dose,struct_split[...,5],num_bins)
    Dmean_ParL = get_DVH_metrics(metrics,dvh_ParL,bins,dose,struct_split[...,5])[0]
    dvh_ParR, bins = get_DVH(dose,struct_split[...,6],num_bins)
    Dmean_ParR = get_DVH_metrics(metrics,dvh_ParR,bins,dose,struct_split[...,6])[0]
    dvh_SubmL, bins = get_DVH(dose,struct_split[...,11],num_bins)
    Dmean_SubmL = get_DVH_metrics(metrics,dvh_SubmL,bins,dose,struct_split[...,11])[0]
    dvh_SubmR, bins = get_DVH(dose,struct_split[...,12],num_bins)
    Dmean_SubmR = get_DVH_metrics(metrics,dvh_SubmR,bins,dose,struct_split[...,12])[0]
    Volume_SubmL = np.sum(struct_split[...,11])
    Volume_SubmR = np.sum(struct_split[...,12])
    return NTCP_definitive_xero_grade3(baseline_score, Dmean_ParL, Dmean_ParR, Dmean_SubmL, Dmean_SubmR, Volume_SubmL, Volume_SubmR)

def ntcp_dysf_2(dose,struct_split):
    # Location: 0: Oral Cavity, 1: Pharynx, 2: Larynx
    location=1
    metrics=['Dmean']
    num_bins = 800
    dvh_Coral, bins = get_DVH(dose,struct_split[...,4],num_bins)
    Dmean_Coral = get_DVH_metrics(metrics,dvh_Coral,bins,dose,struct_split[...,4])[0]
    dvh_PCMSup, bins = get_DVH(dose,struct_split[...,9],num_bins)
    Dmean_PCMSup = get_DVH_metrics(metrics,dvh_PCMSup,bins,dose,struct_split[...,9])[0]
    dvh_PCMMed, bins = get_DVH(dose,struct_split[...,8],num_bins)
    Dmean_PCMMed = get_DVH_metrics(metrics,dvh_PCMMed,bins,dose,struct_split[...,8])[0]
    dvh_PCMInf, bins = get_DVH(dose,struct_split[...,7],num_bins)
    Dmean_PCMInf = get_DVH_metrics(metrics,dvh_PCMInf,bins,dose,struct_split[...,7])[0]
    return NTCP_definitive_dysf_grade2(baseline_score, Dmean_Coral, Dmean_PCMSup, Dmean_PCMMed, Dmean_PCMInf, location)
    
def ntcp_dysf_3(dose,struct_split):
    # Location: 0: Oral Cavity, 1: Pharynx, 2: Larynx 
    location=1
    metrics=['Dmean']
    num_bins = 800
    dvh_Coral, bins = get_DVH(dose,struct_split[...,4],num_bins)
    Dmean_Coral = get_DVH_metrics(metrics,dvh_Coral,bins,dose,struct_split[...,4])[0]
    dvh_PCMSup, bins = get_DVH(dose,struct_split[...,9],num_bins)
    Dmean_PCMSup = get_DVH_metrics(metrics,dvh_PCMSup,bins,dose,struct_split[...,9])[0]
    dvh_PCMMed, bins = get_DVH(dose,struct_split[...,8],num_bins)
    Dmean_PCMMed = get_DVH_metrics(metrics,dvh_PCMMed,bins,dose,struct_split[...,8])[0]
    dvh_PCMInf, bins = get_DVH(dose,struct_split[...,7],num_bins)
    Dmean_PCMInf = get_DVH_metrics(metrics,dvh_PCMInf,bins,dose,struct_split[...,7])[0]
    return NTCP_definitive_dysf_grade3(baseline_score, Dmean_Coral, Dmean_PCMSup, Dmean_PCMMed, Dmean_PCMInf, location)

def patient_referred_to_pt(patient,dose_pt,dose_vmat,struct_split_pt):
    # separate struct
    delta_ntcp_xero_2 = ntcp_xero_2(dose_vmat, struct_split_pt) - ntcp_xero_2(dose_pt, struct_split_pt) 
    delta_ntcp_xero_3 = ntcp_xero_3(dose_vmat, struct_split_pt) - ntcp_xero_3(dose_pt, struct_split_pt)

    # If tumor location is different, we will have to add it to the function parameters
    delta_ntcp_dysf_2 = ntcp_dysf_2(dose_vmat, struct_split_pt) - ntcp_dysf_2(dose_pt, struct_split_pt)
    delta_ntcp_dysf_3 = ntcp_dysf_3(dose_vmat, struct_split_pt) - ntcp_dysf_3(dose_pt, struct_split_pt)
    
    if delta_ntcp_xero_2>=0.1:
        print('patient',patient,'should follow PT treatment')
        return True
    elif delta_ntcp_xero_3>=0.05:
        print('patient',patient,'should follow PT treatment')
        return True 
    elif delta_ntcp_dysf_2>=0.1:
        print('patient',patient,'should follow PT treatment')
        return True
    elif delta_ntcp_dysf_3>=0.05:
        print('patient',patient,'should follow PT treatment')
        return True
    elif delta_ntcp_xero_2+delta_ntcp_dysf_2>=0.15 and delta_ntcp_xero_2>=0.05 and delta_ntcp_dysf_2>=0.05:
        print('patient',patient,'should follow PT treatment')
        return True
    elif delta_ntcp_xero_3+delta_ntcp_dysf_3>=0.075 and delta_ntcp_xero_3>=0.025 and delta_ntcp_dysf_3>=0.025:
        print('patient',patient,'should follow PT treatment')
        return True
    else:
        print('patient',patient,'should follow XT treatment')
        return False
    
# predicted_true
pt_pt = []
pt_xt = []
xt_xt = []
xt_pt = []
for k in range(nb_crossval):
    
    patientlist = np.load(os.path.join(predfolder_pt,params2name(params),'cross_val_k'+str(k+1),'testlist.nii'))
    for patient in patientlist:
        print(patient)
        pred_pt = nib.load(os.path.join(patients_pt,patient,'dose.nii.gz')).get_fdata()
        pred_vmat = nib.load(os.path.join(patients_vmat,patient,'dose.nii.gz')).get_fdata()
        true_pt = nib.load(os.path.join(patients_pt_gt,patient,innerfile,'dose.nii.gz')).get_fdata()
        true_vmat = nib.load(os.path.join(patients_vmat_gt,patient,innerfile,'dose.nii.gz')).get_fdata()
        d0_pt = nib.load(os.path.join(patients_pt_gt,patient,innerfile,'prior_knowledge.nii.gz')).get_fdata()
        d0_vmat = nib.load(os.path.join(patients_vmat_gt,patient,innerfile,'prior_knowledge.nii.gz')).get_fdata()
        
        struct_pt_nib = nib.load(os.path.join(patients_pt_gt,patient,innerfile,'struct_oar.nii.gz'))
        struct_vmat_nib = nib.load(os.path.join(patients_vmat_gt,patient,innerfile,'struct_oar.nii.gz'))
        
        struct_pt = struct_pt_nib.get_fdata().astype(np.int32)
        voxelsize_pt = struct_pt_nib.header.pixdim[1:4]
        voxelsize_xt = struct_vmat_nib.header.pixdim[1:4]
        struct_split_pt = np.zeros((pred_pt.shape[0],pred_pt.shape[1],pred_pt.shape[2],len(struct_labels)),dtype=bool)
        contours_exist_pt = struct_pt_nib.header.extensions[0].get_content()    
        for i in range(len(contours_exist_pt)):
            if contours_exist_pt[i] == 1:   
                struct_split_pt[:, :, :, i] = np.bitwise_and(struct_pt, 2**(i)).astype(bool)
        criteria_pt = [ntcp_xero_2(pred_pt, struct_split_pt),
                    ntcp_xero_3(pred_pt, struct_split_pt),
                    ntcp_dysf_2(pred_pt, struct_split_pt),
                    ntcp_dysf_3(pred_pt, struct_split_pt)]
        criteria_vmat = [ntcp_xero_2(pred_vmat, struct_split_pt),
                    ntcp_xero_3(pred_vmat, struct_split_pt),
                    ntcp_dysf_2(pred_vmat, struct_split_pt),
                    ntcp_dysf_3(pred_vmat, struct_split_pt)]
        np.savez_compressed(os.path.join(result_folder_pt,patient+'_ntcp_output.npz'),criteria_pt)
        np.savez_compressed(os.path.join(result_folder_vmat,patient+'_ntcp_output.npz'),criteria_vmat)
        criteria_gt_vmat = [ntcp_xero_2(true_vmat, struct_split_pt),
                            ntcp_xero_3(true_vmat, struct_split_pt),
                            ntcp_dysf_2(true_vmat, struct_split_pt),
                            ntcp_dysf_3(true_vmat, struct_split_pt)]
        criteria_gt_pt   = [ntcp_xero_2(true_pt, struct_split_pt),
                            ntcp_xero_3(true_pt, struct_split_pt),
                            ntcp_dysf_2(true_pt, struct_split_pt),
                            ntcp_dysf_3(true_pt, struct_split_pt)]
        
        np.savez_compressed(os.path.join(result_folder_pt_gt,patient+'_ntcp_output.npz'),criteria_gt_pt)
        np.savez_compressed(os.path.join(result_folder_vmat_gt,patient+'_ntcp_output.npz'),criteria_gt_vmat)
        criteria_d0_vmat = [ntcp_xero_2(d0_vmat, struct_split_pt),
                            ntcp_xero_3(d0_vmat, struct_split_pt),
                            ntcp_dysf_2(d0_vmat, struct_split_pt),
                            ntcp_dysf_3(d0_vmat, struct_split_pt)]
        criteria_d0_pt   = [ntcp_xero_2(d0_pt, struct_split_pt),
                            ntcp_xero_3(d0_pt, struct_split_pt),
                            ntcp_dysf_2(d0_pt, struct_split_pt),
                            ntcp_dysf_3(d0_pt, struct_split_pt)]
        
        np.savez_compressed(os.path.join(result_folder_pt_d0,patient+'_ntcp_output.npz'),criteria_d0_pt)
        np.savez_compressed(os.path.join(result_folder_vmat_d0,patient+'_ntcp_output.npz'),criteria_d0_vmat)
        gt_pt_ref = patient_referred_to_pt(patient,true_pt,true_vmat,struct_split_pt)
        pred_pt_ref = patient_referred_to_pt(patient,pred_pt,pred_vmat,struct_split_pt)
        if gt_pt_ref:
            if pred_pt_ref:
                pt_pt.append(patient)
            else:
                xt_pt.append(patient)
        else:
            if pred_pt_ref:
                pt_xt.append(patient)
            else:
                xt_xt.append(patient)
nb_patients = len(pt_pt)+len(xt_pt)+len(pt_xt)+len(xt_xt)        
print("True protons", pt_pt)
print("True VMAT", xt_xt)
print("Pred PT but GT is VMAT", pt_xt)
print("Pred VMAT but GT is PT", xt_pt)
print("Correctly referred: {}/{}".format(len(pt_pt)+len(xt_xt),nb_patients))
print("GT referred to PT: {}/{}".format(len(pt_pt)+len(xt_pt),nb_patients))
print("PRED referred to PT: {}/{}".format(len(pt_pt)+len(pt_xt),nb_patients))

def sup_lines(x, y, width, color, linestyle, ax, label):
    for i in range(len(x)):
        ax.hlines(y[i], x[i]-width/2, x[i]+width/2, colors=color, linestyle=linestyle, label=label)

# False Positive and False Negative patients Pred GT
thresholds = [0.1,0.05,0.1,0.05]
# FP
fig, ax = plt.subplots(figsize=[3.4,2])

width = 0.35
for i, patient in enumerate(pt_xt):
    patient_ntcp_pt_pred = np.load(os.path.join(result_folder_pt,patient+'_ntcp_output.npz'))['arr_0']
    patient_ntcp_vmat_pred = np.load(os.path.join(result_folder_vmat,patient+'_ntcp_output.npz'))['arr_0']
    patient_ntcp_pt_gt = np.load(os.path.join(result_folder_pt_gt,patient+'_ntcp_output.npz'))['arr_0']
    patient_ntcp_vmat_gt = np.load(os.path.join(result_folder_vmat_gt,patient+'_ntcp_output.npz'))['arr_0']
    delta_ntcp_pred = patient_ntcp_vmat_pred-patient_ntcp_pt_pred
    delta_ntcp_gt = patient_ntcp_vmat_gt-patient_ntcp_pt_gt
    ax.grid(True)
    ax.bar(np.arange(4) - width/2, delta_ntcp_pred, width, label='\u0394NTCP of predicted doses')
    ax.bar(np.arange(4) + width/2, delta_ntcp_gt, width, label='\u0394NTCP of clinical doses')
    sup_lines(np.arange(4), thresholds, width*2, color='g', linestyle='--', ax=ax, label='Dutch thresholds')
    ax.set_title("Patient {}".format(i+1))
    ax.set_yticks(np.arange(-0.05,0.11,0.05))
    ax.set_ylim(-0.05,0.15)
    ax.set_yticklabels(["-5","0","5","10"])
    ax.set_xticks(np.arange(4))
    ax.set_xticklabels(["\u0394NTCP\nXero 2","\u0394NTCP\nXero 3","\u0394NTCP\nDysph 2","\u0394NTCP\nDysph 3"])
    ax.set_ylabel("NTCP [%]")
    
plt.tight_layout()  
plt.savefig(os.path.join(figure_folder,"bar_chart_ntcp-th_fp.pdf"), format='pdf', dpi=1000)
plt.savefig(os.path.join(figure_folder,"bar_chart_ntcp-th_fp.png"), format='png', dpi=1000)

# FN
print("nb FN", len(xt_pt))

fig, ax = plt.subplots(1,2,figsize=[6.8,6.2])
for i, patient in enumerate(xt_pt[-2:]):
    print(i)
    patient_ntcp_pt_pred = np.load(os.path.join(result_folder_pt,patient+'_ntcp_output.npz'))['arr_0']
    patient_ntcp_vmat_pred = np.load(os.path.join(result_folder_vmat,patient+'_ntcp_output.npz'))['arr_0']
    patient_ntcp_pt_gt = np.load(os.path.join(result_folder_pt_gt,patient+'_ntcp_output.npz'))['arr_0']
    patient_ntcp_vmat_gt = np.load(os.path.join(result_folder_vmat_gt,patient+'_ntcp_output.npz'))['arr_0']
    delta_ntcp_pred = patient_ntcp_vmat_pred-patient_ntcp_pt_pred
    delta_ntcp_gt = patient_ntcp_vmat_gt-patient_ntcp_pt_gt
    
    ax[i//2,i%2].grid(True)
    ax[i//2,i%2].bar(np.arange(4) - width/2, delta_ntcp_pred, width, label='\u0394NTCP of predicted doses')
    ax[i//2,i%2].bar(np.arange(4) + width/2, delta_ntcp_gt, width, label='\u0394NTCP of clinical doses')
    sup_lines(np.arange(4), thresholds, width*2, color='g', linestyle='--', ax=ax[i//2,i%2], label='Dutch thresholds')
    ax[i//2,i%2].set_ylim(-0.05,0.15)
    ax[i//2,i%2].set_title("Patient {}".format(i+1))
    ax[i//2,i%2].set_yticks(np.arange(-0.05,0.11,0.05))
    ax[i//2,i%2].set_yticklabels(["-5","0","5","10"])
    ax[i//2,i%2].set_xticks(np.arange(4))
    ax[i//2,i%2].set_xticklabels(["\u0394NTCP\nXero 2","\u0394NTCP\nXero 3","\u0394NTCP\nDysph 2","\u0394NTCP\nDysph 3"])
    ax[i//2,i%2].set_ylabel("NTCP [%]")
    ax[i//2,i%2].legend()
# set the spacing between subplots
plt.subplots_adjust(hspace=0.7,wspace=0.25)
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.7),fancybox=True, shadow=True, ncol=5)      
plt.tight_layout() 
plt.savefig(os.path.join(figure_folder,"bar_chart_ntcp-th_fn.pdf"), format='pdf', dpi=1000)
plt.savefig(os.path.join(figure_folder,"bar_chart_ntcp-th_fn.png"), format='png', dpi=1000)


