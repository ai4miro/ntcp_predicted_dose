'''
Created on Sep 20, 2017

@Authors by Margerie Huet Dastarac and Camille Draguet Nov, 2020
    
    
'''


from __future__ import print_function, division
from keras import backend as K

def mse_test(y_true, y_pred):
        predicted_dose = y_pred[...,0]
        groundtruth = y_true[...,0]
        return K.mean(K.square(predicted_dose-groundtruth), axis = (1,2,3))

def mse_final():
    def mse(y_true, y_pred):
        predicted_dose = y_pred[...,0]
        groundtruth = y_true[...,0]
        struct = y_true[...,1:]
        print("ypred shape",y_pred.shape)
        print("struct shape",struct.shape)
        return K.mean(K.square(predicted_dose-groundtruth), axis = (1,2,3))
    return mse

