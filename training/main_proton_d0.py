###############################################################################
#                              Import libraries                               #
###############################################################################


import os
from tensorflow.keras.callbacks import ModelCheckpoint, History
import numpy as np
import time
from train_functions_d0 import load_and_generate_3D_plusCT, print_model_layers
from unet_functions import build_3d_unet
from utils import *
from losses import mse_final

# os.environ["CUDA_VISIBLE_DEVICES"]="1"
###############################################################################
#                              User data                                      #
###############################################################################

def main_func():
  # Paths - To change with the corresponding paths in your folders  
  datafolder      =   r'/path/to/patient/PT/data/in/nifti' 
  outputfolder    =   r'/path/to/output/folder/'

  # network parameters
  params = dict(learn_rate =     3e-4,
                loss_function =  mse_final,
                number_of_pool = 4,
                img_rows =       96, 
                img_cols =       96,
                img_slcs =       64,
                dense_enc_dec =  True,
                instance_norm =  True,
                dilate_bottleneck = False
              )
  if not os.path.exists(outputfolder):
    os.makedirs(outputfolder)
  if not os.path.exists(os.path.join(outputfolder, params2name(params))):
    os.makedirs(os.path.join(outputfolder, params2name(params)))
  outputfolder = os.path.join(outputfolder, params2name(params))

  # number of epochs
  epochs= 150
  batch_size =     1
  
  # number of folds for cross-validation
  k = 11
# get test list
  patient_dict = np.load('patients_partition_dict.npy',allow_pickle=True).item()
  testlist_out = patient_dict['out']
  np.save(os.path.join(outputfolder,"testlist_out.npy"),testlist_out) 
  # specify subfolder inside the patient folder
  innerfolder = 'inner_folder_name'

  ###############################################################################
  #                              Run cross-validation                           #
  ###############################################################################

  # upload structure list
  structlist=np.load(os.path.join(datafolder,'trainliststruct_oar.npy')).astype(str).tolist()
  print(structlist)    


  # loop over folds
  for i in range(k):
      
      fold=i+1
      
      print('Cross validation k = '+str(fold))
      
      # create output dir
      output_folder=os.path.join(outputfolder,'cross_val_k'+str(fold))
      if not os.path.exists(output_folder):
        os.mkdir(output_folder)
      
        
      # get val and train patients
      trainlist = patient_dict[i]['train']
      vallist = patient_dict[i]['val']
      testlist = patient_dict[i]['test']
      np.save(os.path.join(output_folder,"vallist.npy"),vallist) 
      np.save(os.path.join(output_folder,"testlist.npy"),testlist) 
      
      # Building the model
      # channels_in --> d0 + ct + tv + oars. The -1 is to remove the body                          
      model = build_3d_unet(  channels_in=2+len(structlist)-1+1,
                              channels_out=1,
                              filter_number=16,
                              kernel_size=(3,3,3),
                              pool_size=(2,2,2),
                              dropout_rate=0,
                              decay_rate=0, **params) 
      
      #print parameters and structure of layers
      print(model.count_params())
      print_model_layers(model)
      
      history = History()

      model_checkpoint=ModelCheckpoint(os.path.join(output_folder,'model-{epoch:02d}-{val_loss:.2f}.h5'), monitor='val_loss', save_best_only=True, save_weights_only=True)
      
      
      verbose=1
      stdev=3
      chkpts=[history, model_checkpoint]
      
      # train model
      t1=time.time()
      model.fit(load_and_generate_3D_plusCT(datafolder=datafolder,
                                            innerfolder=innerfolder,
                                            batchlist=trainlist,
                                            batchsize=batch_size,
                                            numrows=params["img_rows"],
                                            numcols=params["img_cols"],
                                            numslcs=params["img_slcs"],
                                            stage=TRAINING_FLIP),
                                  steps_per_epoch=round(len(trainlist)/batch_size),
                                  epochs= epochs,
                                  verbose=verbose,
                                  callbacks=chkpts,
                                  validation_data=load_and_generate_3D_plusCT(datafolder=datafolder,
                                                                              innerfolder=innerfolder,
                                                                              batchlist=vallist,
                                                                              batchsize=batch_size,
                                                                              numrows=params["img_rows"],
                                                                              numcols=params["img_cols"],
                                                                              numslcs=params["img_slcs"],
                                                                              stage=VALIDATION_PATCHES),
                                  validation_steps=round(len(vallist)))
      print("training time :", time.time()-t1)
      # save model
      np.save(os.path.join(output_folder,'history%04d'%(epochs)+'.npy'),history.history)
      save_learning_curve(output_folder,hist_path = os.path.join(output_folder,'history%04d'%(epochs)+'.npy'),continue_training=continue_training)


main_func()
