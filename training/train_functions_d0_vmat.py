"""

@authors:  Margerie Huet Dastarac
"""
import numpy as np
import nibabel as nib
import os
import random
from scipy import ndimage
from utils import *


def image_transform(image, angles=None, shifts=None, flip=None, transform=None, order=None):
    """ 
    Inspired from Eliott Brion 's work: https://github.com/eliottbrion/pelvis_segmentation/blob/master/data_generators.py
    Transforms an input 3d-matrix:
    - Rotation of angle=angle according to X-axis (antero-posterior axis).
    - Shifts of a different amount in every direction.
    Those two operations are performed thanks to an interpolation of order=order.
    - Flip indicates whether the matrix should be then shifted along Y-axis (longitudinal axis).

    :params image: (3D array) to transform. Shape: (img_rows, img_cols, img_slcs)
    :param angles: (1D array) rotation angle. Shape: (1,)
    :param shifts: (1D array) shift values in every direction. Shape: (3,)
    :param flip: (int) 0 if image must not be flipped, 1 if image must be flipped
    :param transform: (int) 0 if image must not be rotated and shifted, 1 if it must
    :param order: (int) order of the interpolation needed to apply the transformations
    :return: image: (3D array) transformed image. Shape: (img_rows, img_cols, img_slcs)

    """
    if transform:
        image_size = image.shape
        # Translation, matrix of dimension n+1
        shift_matrix = np.array([[1, 0, 0, shifts[0]],
                            [0, 1, 0, shifts[1]],
                            [0, 0, 1, shifts[2]],
                            [0, 0, 0, 1]])

        # Offset for rotation wrt. the center
        offset = np.array([[1, 0, 0, -int(image_size[0]/2)],
                    [0, 1, 0, -int(image_size[1]/2)],
                    [0, 0, 1, -int(image_size[2]/2)],
                    [0, 0, 0, 1]])

        offset_opp = np.array([[1, 0, 0, int(image_size[0]/2)],
                    [0, 1, 0, int(image_size[1]/2)],
                    [0, 0, 1, int(image_size[2]/2)],
                    [0, 0, 0, 1]])

        angles = np.deg2rad(angles)
        rotx = np.array([[1, 0, 0, 0],
                    [0, np.cos(angles[0]), -np.sin(angles[0]), 0],
                    [0, np.sin(angles[0]), np.cos(angles[0]), 0],
                    [0, 0, 0, 1]])
        
        rotation_matrix = offset_opp.dot(rotx).dot(offset)
        affine_matrix = shift_matrix.dot(rotation_matrix)

        image = ndimage.interpolation.affine_transform(image, affine_matrix, order=order, mode='nearest')

    if flip:
        image = np.flip(image,axis=1)
    return image
    
def load_and_generate_3D_plusCT(datafolder=None,innerfolder = None, batchlist=None,batchsize=32,numrows=64,numcols=64,numslcs=64,stage=None):
    """
    Load data and generate (x,y) pairs for the training and validation phase and (x) for testing phase.
    
    :param datafolder: (str) path where patients are located
    :param batchlist: (array of str) patients that can be used for training, validation or testing
    :param batchsize: (int) nb of patients in the batch
    :param numrows,numcols,numslcs: (int,int,int) patch dimensions
    :param stage: (int) stage btw training, validation and testing
    :return: (xreturn, yreturn): (input, output) pair for training and validation, (xreturn): input for testing    
    """
    numdim=np.array([numrows,numcols,numslcs])    
    while True:
        arrayshape = {}
        contoursexist = {}
        x_ct = {}
        x_prior_knowledge = {}
        x_oar_together = {}
        x_ptv_together = {}
        y_dose = {}
        
        x_oar_split = {}
        x_ptv_split = {}
        
        mask = {}
        x_oar_together_transformed = {}
        x_oar_split_transformed = {}
        x_ptv_together_transformed = {}
        x_ptv_split_transformed = {}
        mask_transformed={}

        probability = {}
        sample_probability = {}
        
        num_oar_total = {}
        num_ptv_total = {}
        num_ct=0
        
            
        xreturn = np.zeros((batchsize*2 if stage==TRAINING or stage==TRAINING_FLIP else batchsize, numrows, numcols, numslcs, 1+1+num_ct+14),dtype=np.float32)
        
        yreturn = np.zeros((batchsize*2 if stage==TRAINING or stage==TRAINING_FLIP else batchsize, numrows, numcols, numslcs, 1+1+15),dtype=np.float32)# + 1 + 14),dtype=np.float32)
        
        countbatchsize=0
        for patient in np.random.choice(batchlist, batchsize, replace=False):
            srcfolder = os.path.join(datafolder, patient, innerfolder)
            x_prior_knowledge[patient] = nib.load(os.path.join(srcfolder, 'prior_knowledge.nii.gz')).get_fdata()
            struct_nib = nib.load(os.path.join(srcfolder, 'struct_oar.nii.gz'))# OARs encoded in a single 3d-matrix
            x_oar_together[patient] = struct_nib.get_fdata().astype(np.int32)
            x_ptv_together[patient] = nib.load(os.path.join(srcfolder, 'struct_tv.nii.gz')).get_fdata()# if os.path.exists(np.load(os.path.join(srcfolder, 'ctv.npy'))) else np.load(os.path.join(srcfolder, 'ptv.npy'))
            
            ct_nib                 = nib.load(os.path.join(srcfolder,'ct.nii.gz'))
            ct = ct_nib.get_fdata()
            x_ct[patient]      = (ct-ct.min()) / (ct.max()-ct.min()) # scale between 0 and 1
            arrayshape[patient]    = ct_nib.header['dim'][1:4]
            contoursexist[patient] = struct_nib.header.extensions[0].get_content()    
            
            if stage!=TEST: y_dose[patient] = nib.load(os.path.join(srcfolder, 'dose.nii.gz')).get_fdata()

            num_oar_total[patient] = len(contoursexist[patient])
            print(num_oar_total[patient])
            
            # Create an array that stores the mask for each OAR separately
            x_oar_split[patient] = np.zeros((arrayshape[patient][0],arrayshape[patient][1],arrayshape[patient][-1],num_oar_total[patient]),dtype=np.bool)
            for i in range(len(contoursexist[patient])-1):
                if contoursexist[patient][i] == 0:
                    continue
                x_oar_split[patient][:, :, :, i] = np.bitwise_and(x_oar_together[patient], 2**(i)).astype(bool)
            
            # Create an array that stores the mask for each PTV separately, necessary to avoid distorsions in the image_transform function
            unique_ptv = np.unique(x_ptv_together[patient])
            
            unique_ptv = unique_ptv[1:]
            num_ptv_total[patient]=len(unique_ptv)
            x_ptv_split[patient]=np.zeros((arrayshape[patient][0],arrayshape[patient][1],arrayshape[patient][-1],num_ptv_total[patient]),dtype=int)            
            for i in range(num_ptv_total[patient]): 
                x_ptv_split[patient][:,:,:,i][x_ptv_together[patient]==unique_ptv[i]]=1 #Binary masks
            
            if stage==TRAINING_FLIP:
                sample_probability[patient] =[]
                sample_probability[patient].append(np.load(os.path.join(srcfolder, 'sample_probability_row.npz'))['arr_0'])
                sample_probability[patient].append(np.load(os.path.join(srcfolder, 'sample_probability_col.npz'))['arr_0'])
                sample_probability[patient].append(np.load(os.path.join(srcfolder, 'sample_probability_slc.npz'))['arr_0'])

                # Patch probabilities
                for i in range(len(sample_probability[patient])):
                    sample_probability[patient][i] = np.power(1000 * sample_probability[patient][i], 2)
                    sample_probability[patient][i] = sample_probability[patient][i] / np.sum(sample_probability[patient][i])

                probability[patient]=[]
                for dim in range(len(numdim)):
                    probability[patient].append(np.zeros(len(sample_probability[patient][dim]) - numdim[dim]))
                    for i in range(len(probability[patient][dim])):
                        probability[patient][dim][i] = np.sum(sample_probability[patient][dim][i:i + numdim[dim]])
                    probability[patient][dim]=probability[patient][dim]/np.sum(probability[patient][dim])

                row=np.random.choice(np.arange(0, len(probability[patient][0])), p=probability[patient][0])
                col=np.random.choice(np.arange(0, len(probability[patient][1])), p=probability[patient][1])
                slc=np.random.choice(np.arange(0, len(probability[patient][2])), p=probability[patient][2])
                
                #Random flip:
                flip = random.uniform(0, 1)
                
                mask[patient]=np.zeros((arrayshape[patient][0],arrayshape[patient][1],arrayshape[patient][-1],num_oar_total[patient]+1))#num_ptv_total[patient]))
                mask[patient][:,:,:,0:num_oar_total[patient]]=x_oar_split[patient]
                mask[patient][:,:,:,num_oar_total[patient]]=x_ptv_together[patient]
                if input_ct=='CT_WET':
                    xreturn[countbatchsize,:,:,:,0:2]=x_ct[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                    xreturn[countbatchsize,:,:,:,2]=x_ptv_together[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                    xreturn[countbatchsize,:,:,:,3:3+num_oar_total[patient]]=x_oar_split[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs,:]
                else:
                    xreturn[countbatchsize,:,:,:,0]=x_prior_knowledge[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                    xreturn[countbatchsize,:,:,:,1]=x_ct[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                    xreturn[countbatchsize,:,:,:,2]=x_ptv_together[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                    xreturn[countbatchsize,:,:,:,3:3+num_oar_total[patient]-1]=x_oar_split[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs,1:] #not body
                
                yreturn[countbatchsize,:,:,:,0]=y_dose[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                yreturn[countbatchsize,:,:,:,1:1+num_oar_total[patient]+1]=mask[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs,:]
                
                #Flip or no flip? 
                xreturn[countbatchsize,:,:,:,:]=image_transform(xreturn[countbatchsize,:,:,:,:], angles=None, shifts=None, flip=flip, transform=0, order=1).astype(int)
                yreturn[countbatchsize,:,:,:,:]=image_transform(yreturn[countbatchsize,:,:,:,:],  angles=None, shifts=None, flip=flip, transform=0, order=1).astype(int)
                
                countbatchsize += 1
            elif stage==TRAINING:
                ### Original patient
                mask[patient]=np.zeros((arrayshape[patient][0],arrayshape[patient][1],arrayshape[patient][-1],num_oar_total[patient]+1))
                mask[patient][:,:,:,0:num_oar_total[patient]]=x_oar_split[patient]
                mask[patient][:,:,:,num_oar_total[patient]]=x_ptv_together[patient]

                xreturn[countbatchsize,:,:,:,0]=x_prior_knowledge[patient]
                xreturn[countbatchsize,:,:,:,1]=x_ct[patient]
                xreturn[countbatchsize,:,:,:,2]=x_ptv_together[patient]
                xreturn[countbatchsize,:,:,:,3:3+num_oar_total[patient]]=x_oar_split[patient]
                yreturn[countbatchsize,:,:,:,0]=y_dose[patient]
                yreturn[countbatchsize,:,:,:,1:1+num_oar_total[patient]+1]=mask[patient]

                ### Fliped and transformed patient
                angles = np.array([5*random.uniform(-1,1)])
                shifts = np.array([0.05*random.uniform(-1,1)*numdim[i] for i in range(3)])
                flip = random.randint(0,1)

                # Transform OARs
                x_oar_split_transformed[patient]=np.zeros((arrayshape[patient][0],arrayshape[patient][1],arrayshape[patient][-1],num_oar_total[patient]))
                for i in range(num_oar_total[patient]):
                    x_oar_split_transformed[patient][:,:,:,i]=image_transform(x_oar_split[patient][:,:,:,i], angles, shifts, flip=1, transform=0, order=1)
                
                # Transform PTVs and regroup them after transformation
                x_ptv_split_transformed[patient]=np.zeros((arrayshape[patient][0],arrayshape[patient][1],arrayshape[patient][-1],num_ptv_total[patient]),dtype=int)
                x_ptv_together_transformed[patient]=np.zeros((arrayshape[patient][0],arrayshape[patient][1],arrayshape[patient][-1]))
                for i in range(num_ptv_total[patient]):
                    x_ptv_split_transformed[patient][:,:,:,i]=image_transform(x_ptv_split[patient][:,:,:,i], angles, shifts, flip=1, transform=0, order=1).astype(int)
                    x_ptv_together_transformed[patient][x_ptv_split_transformed[patient][:,:,:,i]]=unique_ptv[i]

                mask_transformed[patient]=np.zeros((arrayshape[patient][0],arrayshape[patient][1],arrayshape[patient][-1],num_oar_total[patient]+1))
                mask_transformed[patient][:,:,:,0:num_oar_total[patient]]=x_oar_split_transformed[patient]
                mask_transformed[patient][:,:,:,num_oar_total[patient]]=x_ptv_together_transformed[patient]
                
                xreturn[countbatchsize+1,:,:,:,0]=image_transform(x_ct[patient], angles, shifts, flip=1, transform=0, order=3)
                xreturn[countbatchsize+1,:,:,:,1]=x_ptv_together_transformed[patient]
                xreturn[countbatchsize+1,:,:,:,2:2+num_oar_total[patient]]=x_oar_split_transformed[patient]
                yreturn[countbatchsize+1,:,:,:,0]=image_transform(y_dose[patient], angles, shifts, flip=1, transform=0, order=1)
                yreturn[countbatchsize+1,:,:,:,1:1+num_oar_total[patient]+1]=mask_transformed[patient]

                countbatchsize += 2 #batch augmentation with transformed patient
                del mask_transformed, x_ptv_together_transformed, x_ptv_split_transformed, x_oar_split_transformed

            elif stage==VALIDATION:
                mask[patient]=np.zeros((arrayshape[patient][0],arrayshape[patient][1],arrayshape[patient][-1],num_oar_total[patient]+1))#num_ptv_total[patient]))
                mask[patient][:,:,:,0:num_oar_total[patient]]=x_oar_split[patient]
                mask[patient][:,:,:,num_oar_total[patient]]=x_ptv_together[patient]
                if input_ct=='CT_WET':
                    xreturn[countbatchsize,:,:,:,0:2]=x_ct[patient]
                    xreturn[countbatchsize,:,:,:,2]=x_ptv_together[patient]
                    xreturn[countbatchsize,:,:,:,3:3+num_oar_total[patient]]=x_oar_split[patient]
                else:
                    xreturn[countbatchsize,:,:,:,0]=x_prior_knowledge[patient]
                    xreturn[countbatchsize,:,:,:,1]=x_ct[patient]
                    xreturn[countbatchsize,:,:,:,2]=x_ptv_together[patient]
                    xreturn[countbatchsize,:,:,:,3:3+num_oar_total[patient]]=x_oar_split[patient]
                
                yreturn[countbatchsize,:,:,:,0]=y_dose[patient]
                yreturn[countbatchsize,:,:,:,1:]=mask[patient]
                countbatchsize += 1
            elif stage==VALIDATION_PATCHES:
                sample_probability[patient] =[]
                sample_probability[patient].append(np.load(os.path.join(srcfolder, 'sample_probability_row.npz'))['arr_0'])
                sample_probability[patient].append(np.load(os.path.join(srcfolder, 'sample_probability_col.npz'))['arr_0'])
                sample_probability[patient].append(np.load(os.path.join(srcfolder, 'sample_probability_slc.npz'))['arr_0'])

                # Patch probabilities
                for i in range(len(sample_probability[patient])):
                    sample_probability[patient][i] = np.power(1000 * sample_probability[patient][i], 2)
                    sample_probability[patient][i] = sample_probability[patient][i] / np.sum(sample_probability[patient][i])

                probability[patient]=[]
                for dim in range(len(numdim)):
                    probability[patient].append(np.zeros(len(sample_probability[patient][dim]) - numdim[dim]))
                    for i in range(len(probability[patient][dim])):
                        probability[patient][dim][i] = np.sum(sample_probability[patient][dim][i:i + numdim[dim]])
                    probability[patient][dim]=probability[patient][dim]/np.sum(probability[patient][dim])

                row=np.random.choice(np.arange(0, len(probability[patient][0])), p=probability[patient][0])
                col=np.random.choice(np.arange(0, len(probability[patient][1])), p=probability[patient][1])
                slc=np.random.choice(np.arange(0, len(probability[patient][2])), p=probability[patient][2])
                
                mask[patient]=np.zeros((arrayshape[patient][0],arrayshape[patient][1],arrayshape[patient][-1],num_oar_total[patient]+1))#num_ptv_total[patient]))
                mask[patient][:,:,:,0:num_oar_total[patient]]=x_oar_split[patient]
                mask[patient][:,:,:,num_oar_total[patient]]=x_ptv_together[patient]
                if input_ct=='CT_WET':
                    xreturn[countbatchsize,:,:,:,0:2]=x_ct[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                    xreturn[countbatchsize,:,:,:,2]=x_ptv_together[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                    xreturn[countbatchsize,:,:,:,3:3+num_oar_total[patient]]=x_oar_split[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs,:]
                else:
                    xreturn[countbatchsize,:,:,:,0]=x_prior_knowledge[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                    xreturn[countbatchsize,:,:,:,1]=x_ct[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                    xreturn[countbatchsize,:,:,:,2]=x_ptv_together[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                    xreturn[countbatchsize,:,:,:,3:3+num_oar_total[patient]-1]=x_oar_split[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs,1:] #not body
                
                yreturn[countbatchsize,:,:,:,0]=y_dose[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs]
                yreturn[countbatchsize,:,:,:,1:1+num_oar_total[patient]+1]=mask[patient][row:row+numrows,col:col+numcols,slc:slc+numslcs,:]
                
                countbatchsize += 1
            else:
                xreturn[countbatchsize,:,:,:,0]=x_prior_knowledge[patient]
                xreturn[countbatchsize,:,:,:,1]=x_ct[patient]
                xreturn[countbatchsize,:,:,:,2]=x_ptv_together[patient]
                xreturn[countbatchsize,:,:,:,3:3+num_oar_total[patient]-1]=x_oar_split[patient][1:]
                countbatchsize += 1
        del x_prior_knowledge, x_ct, x_ptv_together, x_ptv_split, x_oar_together, x_oar_split,arrayshape, mask, num_oar_total, num_ptv_total
        
        if stage==TEST:
            yield xreturn
        else:
            
            yield (xreturn,yreturn)



    
