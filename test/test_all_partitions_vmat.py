import os, glob
import numpy as np
import nibabel as nib
from losses import mse_final
from utils import *
from unet_functions import build_3d_unet

os.environ["CUDA_VISIBLE_DEVICES"]="0" # For 2 gpu: "0,1"

tv_value = [] # Add TV prescription values in float in the list

def load_patient_for_test(datafolder,patient,filefolder):
    """
    Load patient data for test
    :param datafolder: (str) path where patients are located
    :param patient: (str) name of the patient
    :param filefolder: (str) inner folder inside the patient dir where data is located

    :return x: (tensor) with all input data
    :return mask_output: (tensor) with boolean mask for each ROI 
    """
            
    # folder to load patient data
    srcfolder = os.path.join(datafolder,patient,filefolder) 
    # get prior knowledge target
    x_prior_knowledge = nib.load(os.path.join(srcfolder, 'prior_knowledge.nii.gz')).get_fdata()
    # get OARs
    struct_nib = nib.load(os.path.join(srcfolder, 'struct_oar.nii.gz'))# OARs encoded in a single 3d-matrix
    x_oar_together = struct_nib.get_fdata().astype(np.int32)
    # get TVs
    x_tv_together = nib.load(os.path.join(srcfolder, 'struct_tv.nii.gz')).get_fdata()# if os.path.exists(np.load(os.path.join(srcfolder, 'ctv.npy'))) else np.load(os.path.join(srcfolder, 'ptv.npy'))
    # get CT
    ct_nib                 = nib.load(os.path.join(srcfolder,'ct.nii.gz'))
    ct = ct_nib.get_fdata()
    # get shape
    arrayshape    = ct_nib.header['dim'][1:4]
    # get existing OARs contours
    contours_exist = struct_nib.header.extensions[0].get_content()    
    
    num_oar_total = len(contours_exist)
    print('num_oar_total',num_oar_total)
    
    # split OARs
    x_oar_split = np.zeros((arrayshape[0],arrayshape[1],arrayshape[2],num_oar_total))
    for c in range(num_oar_total):
        if not contours_exist[c]:
            continue
        x_oar_split[:,:,:,c]=np.bitwise_and(x_oar_together,2**(c)).astype(bool)
    
    # split PTVs   
    unique_tv = np.unique(x_tv_together)
    unique_tv = unique_tv[1:]
    tv_exist=np.zeros((2,))
    j=0
    for i in range(len(tv_value)):
        if tv_value[i]==unique_tv[j]: 
            tv_exist[i]=tv_value[i]
            j+=1
    num_ptv_total=len(tv_exist)
    x_ptv_split=np.zeros((arrayshape[0],arrayshape[1],arrayshape[-1],num_ptv_total),dtype=int)
    for i in range(num_ptv_total): 
        x_ptv_split[:,:,:,i][x_tv_together==tv_value[i]]=1

    # boolean mask needed for mse_per_ROIs function
    maskOutput=np.zeros((arrayshape[0],arrayshape[1],arrayshape[-1],num_oar_total+num_ptv_total),dtype=bool)
    maskOutput[:,:,:,0:num_oar_total]=x_oar_split
    maskOutput[:,:,:,num_oar_total:num_oar_total+num_ptv_total]=x_ptv_split.astype(bool)
 
    # initialize input tensor
    x = np.zeros((arrayshape[0],arrayshape[1],arrayshape[2],1+1+num_oar_total))
    x[:,:,:,0]=x_prior_knowledge
    x[:,:,:,1]=(ct-ct.min()) / (ct.max()-ct.min()) # scale between 0 and 1
    x[:,:,:,2]=x_tv_together
    x[:,:,:,3:3+num_oar_total-1]=x_oar_split[:,:,:,1:]
    
    return x, maskOutput[:,:,:,1:], maskOutput[:,:,:,0] #remove the body

def predict_patchwise(model, x, patch_size, stride):
    """
    predicts using patches.
    :param model: model
    :param x: (tensor) input channels data
    :param patch_size: (int,int,int) size of the 3D patch
    :param stride: (int,int,int) stride for patch prediction
    :return: ypred: (tensor) prediction
    """
    # coordinates
    X, Y, Z = np.meshgrid(np.linspace(-1, 1, patch_size[1]), np.linspace(-1, 1, patch_size[0]),
                          np.linspace(-1, 1, patch_size[2]))
    # build Gaussian
    mu, sigma = 0, 2.5
    G = np.exp(-((X - mu) ** 2 + (Y - mu) ** 2 + (Z - mu) ** 2) / 2.0 * sigma ** 2)

    # voxels indices
    ns_row = np.ceil((x.shape[0] - patch_size[0]) / stride[0]).astype(int)
    ns_col = np.ceil((x.shape[1] - patch_size[1]) / stride[1]).astype(int)
    ns_slc = np.ceil((x.shape[2] - patch_size[2]) / stride[2]).astype(int)

    padrow = ns_row * stride[0] + patch_size[0] - x.shape[0]
    padcol = ns_col * stride[1] + patch_size[1] - x.shape[1]
    padslc = ns_slc * stride[2] + patch_size[2] - x.shape[2]

    npad = ((padrow, 0), (padcol, 0), (padslc, 0), (0, 0))
    x = np.pad(x, pad_width=npad, mode='constant', constant_values=0)
    ypred = np.zeros((x.shape[0], x.shape[1], x.shape[2]))
    yweight = np.zeros((x.shape[0], x.shape[1], x.shape[2]))

    # get patches and merge them
    for i in range(ns_row + 1):
        for j in range(ns_col + 1):
            for k in range(ns_slc + 1):
                row = i * stride[0]
                col = j * stride[1]
                slc = k * stride[2]
                xbuff = np.zeros((1, patch_size[0], patch_size[1], patch_size[2], x.shape[-1]))
                xbuff[0, :, :, :, :] = x[row:row + patch_size[0], col:col + patch_size[1], slc:slc + patch_size[2], :]
                ybuff = model.predict(xbuff)
                print(np.max(ybuff))
                ypred[row:row + patch_size[0], col:col + patch_size[1], slc:slc + patch_size[2]] = (
                np.divide(np.multiply(ypred[row:row + patch_size[0], col:col + patch_size[1], slc:slc + patch_size[2]],
                                      yweight[row:row + patch_size[0], col:col + patch_size[1], slc:slc + patch_size[2]])
                          + np.multiply(ybuff[0, :, :, :, 0], G),
                          G + yweight[row:row + patch_size[0], col:col + patch_size[1], slc:slc + patch_size[2]]))
                yweight[row:row + patch_size[0], col:col + patch_size[1], slc:slc + patch_size[2]] += G
                
    ypred = np.delete(ypred, range(padrow), 0)
    ypred = np.delete(ypred, range(padcol), 1)
    ypred = np.delete(ypred, range(padslc), 2)
    return ypred

def predict(model, x):
    """
    predicts using patches.
    :param model: model
    :param x: (tensor) input channels data
    :return: ypred: (tensor) prediction
    """
    x_exp = np.expand_dims(x,axis=0)
    y = model.predict(x_exp)
    return y.squeeze()


def test():
    """
    Test models on test set, save prediction in npy format and a boxplot of the mse metrics.
    """
    # Parameters of the network
    params = dict(learn_rate =     3e-4,
                loss_function =  mse_final,
                number_of_pool = 4,
                img_rows =       96, 
                img_cols =       96,
                img_slcs =       64,
                dense_enc_dec =  True,
                instance_norm =  True,
                dilate_bottleneck = False
              )
    # Different paths
    # Folder containting the testset
    datafolder = r'/path/to/folder/with/patient/VMAT/data'
    # Output folder of the model training: if it does not exist, exit with an error
    chosen_epoch = [70,82,77,80,95,94,87,76,81,88,81]
    outputfolder = r'/folder/where/trained/models/are/located'
    # Folder where the predictions will be saved
    resultfolder = os.path.join(outputfolder,'test_predictions')
    #Defining struct labels
    target = "CTV" # to be chosen between "CTV" and "PTV"
    struct_labels = np.load(os.path.join(datafolder,'trainliststruct.npy')).tolist()
    struct_labels.extend([target+str(int(p*100)) for p in tv_value])
    print(struct_labels)
    nb_models = 11
    
    out_patients = [] # Add list of outer test patients
    for k in range(nb_models):
        print('path',os.path.join(outputfolder,"cross_val_k"+str(k+1),"model-"+str(chosen_epoch[k])+"*"))
        model_path =  glob.glob(os.path.join(outputfolder,"cross_val_k"+str(k+1),"model-{:02d}*".format(chosen_epoch[k])))
        print(model_path)
        model_path=model_path[0]
        patientlist = np.load(os.path.join(outputfolder,"cross_val_k"+str(k+1),"testlist.npy"))#['ANON242','ANON2666','ANON3027','ANON3435','ANON3753','ANON866','ANON821','ANON590','ANON2200','ANON4612']
        print('k',k,'patients',patientlist)

        # Check if output folder exists. If not, exit and prints the model parameters to find eventual error.
        if not os.path.exists(outputfolder):
            print("Error: that experience may not have been ran on this computer, folder not available.")
            print( params2name(params))
            return -1
        
        # Makes the result folder if does not exist   
        if not os.path.exists(resultfolder):
            os.makedirs(resultfolder)
        
        structlist=np.load(os.path.join(datafolder,'trainliststruct.npy')).astype(str).tolist()
        
        # Build model architecture
        model = build_3d_unet(channels_in=2+len(structlist)+1-1,
                                channels_out=1,
                                filter_number=16,
                                kernel_size=(3,3,3),
                                pool_size=(2,2,2),
                                dropout_rate=0,
                                decay_rate=0, **params)
        # Load weights
        model.load_weights(model_path)
        
        batch_size = 1
        
        if not os.path.exists(os.path.join(resultfolder, "pred_npy")):
            os.makedirs(os.path.join(resultfolder, "pred_npy"))
        if not os.path.exists(os.path.join(resultfolder, "test_body_masks")):
            os.makedirs(os.path.join(resultfolder, "test_body_masks"))
        if not os.path.exists(os.path.join(resultfolder,'dvh')):
            os.makedirs(os.path.join(resultfolder,'dvh'))

        #Save predictions in csv: (image, output_path, subject)
        for i, patient in enumerate(patientlist): 
            if not os.path.exists(os.path.join(resultfolder, "pred_nii",patient)):
                os.makedirs(os.path.join(resultfolder, "pred_nii",patient))
            # Load patient input and struct
            x_test, struct, body = load_patient_for_test(datafolder,patient,innerfile)
            # Predict by patches and merge them to get full image prediction
            y_pred = predict_patchwise(model=model, x=x_test, patch_size=[params['img_rows'],params['img_cols'],params['img_slcs']], stride=[32,32,32])
            # Save nib file of the prediction
            nib.save(y_pred,os.path.join(resultfolder, "pred_nii", patient,'dose.nii.gz'))
            # Save image_position_patient file too
            nib.save(body,os.path.join(resultfolder,'pred_nii',patient,'mask_body.nii.gz'))
            # Load groundtruth to measure mse
            y_true = nib.load(os.path.join(datafolder, patient, innerfile,'dose.nii.gz')).get_fdata()
            mse = np.square(np.subtract(y_pred, y_true)).mean()
            print(patient, mse)
            # mse of each structure 
            compare_dvh_and_get_metrics(struct, struct_labels, y_true, y_pred, metrics=['D95','V5','Dmean','Dmax'], savefile=os.path.join(resultfolder)+patient+'_')
        
        # Performance on out_set
        for i, patient in enumerate(out_patients): 
            if not os.path.exists(os.path.join(resultfolder, "pred_nii",patient)):
                os.makedirs(os.path.join(resultfolder, "pred_nii",patient))
            if not os.path.exists(os.path.join(outputfolder,"cross_val_k"+str(k+1), patient)):
                os.makedirs(os.path.join(outputfolder,"cross_val_k"+str(k+1), patient))
            # Load patient input and struct
            x_test, struct, body = load_patient_for_test(datafolder,patient,innerfile)
            
            # Predict by patches and merge them to get full image prediction
            y_pred = predict_patchwise(model=model, x=x_test, patch_size=[96,96,64], stride=[32,32,32])
            # Save npy file of the prediction
            nib.save(y_pred, os.path.join(outputfolder,"cross_val_k"+str(k+1), patient,'dose.nii.gz'))
            # Save mask_body file too
            nib.save(body,os.path.join(resultfolder,'pred_nii',patient,'mask_body.nii.gz'))
            
            # Load groundtruth to measure mse
            y_true = nib.load(os.path.join(datafolder, patient, innerfile,'dose.nii.gz')).get_fdata()
            # mse of each structure 
            print(os.path.join(outputfolder,"cross_val_k"+str(k+1)+'/')+patient+'_')
            compare_dvh_and_get_metrics(struct, struct_labels[1:], y_true, y_pred, metrics=['D95','V5','Dmean','Dmax'], savefile=os.path.join(outputfolder,"cross_val_k"+str(k+1)+'/')+patient+'_')
        
test()
