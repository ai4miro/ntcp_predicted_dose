import os
import numpy as np
import matplotlib.pyplot as plt
from utils import *

folder_pred_target = r'/auto/globalscratch/users/h/u/huetm/HAN_prediction/patch_expansion_1/learn_rate_0.0003_loss_function_mse_final_number_of_pool_4_img_rows_96_img_cols_96_img_slcs_64_dense_enc_dec_True_instance_norm_True_dilate_bottleneck_False'
folder_pred_target_oar = r'/auto/globalscratch/users/h/u/huetm/HAN_prediction/full_body_expansion_1_interpn/learn_rate_0.0003_loss_function_mse_final_number_of_pool_4_img_rows_112_img_cols_192_img_slcs_144_dense_enc_dec_True_instance_norm_True_dilate_bottleneck_False_expansion_rate_1'
folder_clinic = r'/CECI/proj/miro/HAN_PT_interpn_npz_oar'
# folder_clinic_cropped = r'/CECI/proj/miro/HAN_PT_npz_cropped_interpn/'

prediction_folder_pred_target = os.path.join(folder_pred_target,'test_predictions')
prediction_folder_pred_target_oar = os.path.join(folder_pred_target_oar,'test_predictions')
figure_folder = r'/auto/globalscratch/users/h/u/huetm/HAN_prediction/figures_comparison_pred_d0'
if not os.path.exists(figure_folder):
    os.makedirs(figure_folder)
struct_labels = np.load('trainliststruct.npy')
nb_patients = len(os.listdir(os.path.join(prediction_folder_pred_target,"pred_npy_norm_ctvp")))
target = "CTV" #To be chosen between "CTV" or "PTV"
prescription=[54.25, 70.]
target_labels = [target+str(int(p*100)) for p in prescription]
patientlist = np.load(os.path.join(prediction_folder_pred_target,'pred_npy'))

def build_dvh_metrics_dict(struct_labels, target='CTV', prescription=[54.25, 70.], metrics=['D0.03', 'D2','D5', 'Dmean', 'D95', 'D99', 'Dmax']):
    dvh_metrics = {}
    # Add OAR
    for s in struct_labels:
        dvh_metrics[s]={}
        for m in metrics:
            dvh_metrics[s][m]=[]
    # Add target
    for p in prescription:
        s = target+str(int(p*100))
        dvh_metrics[s]={}
        for m in metrics:
            dvh_metrics[s][m]=[]
    return dvh_metrics

dvh_metrics = build_dvh_metrics_dict(struct_labels = struct_labels)

# Get dictionary for clinical doses
# print("prediction_folder_patch",prediction_folder_patch)
dict_gt = get_dvh_dict_gt(patientlist=patientlist, predictionfolder=prediction_folder_patch,structfolder=folder_clinic,innerfolder="resized_3mm",struct_labels=struct_labels, dvh_metrics = dvh_metrics)
# Get dictionary for predicted doses with patches
dict_prior_knowledge_target = get_dvh_dict_prior_knowledge(patientlist=patientlist, predictionfolder=None,structfolder=folder_clinic,innerfolder="resized_3mm", struct_labels=struct_labels, dvh_metrics=dvh_metrics,prior_knowledge='prior_knowledge_target')
# Get dictionary for predicted doses without patches
dict_prior_knowledge_target_oar = get_dvh_dict_prior_knowledge(patientlist=patientlist, predictionfolder=None,structfolder=folder_clinic, innerfolder="resized_3mm", struct_labels=struct_labels, dvh_metrics=dvh_metrics,prior_knowledge='prior_knowledge_target_oar')
print("dict_gt")
print(dict_gt)
print()
print("dict_patch")
print(dict_patch)
print()
print("dict_full")
print(dict_full)
print()
#####################
#    Boxplot OAR    #
#####################
metrics = ['D2','Dmean','Dmean','Dmean','Dmean','Dmean','Dmean','Dmean','Dmean','D2','Dmean','Dmean','D5']
struct_labels_box = ['Brainstem', 'Esophagus_upper','GlotticArea','OralCavity', 'Parotid_L','Parotid_R','PharConsInf','PharConsMid','PharConsSup','SpinalCord','Submandibular_L','Submandibular_R','SupraglotLarynx']
struct_labels_box_display = ['Brainstem - D2','Esophagus - Dmean','GlotticArea - Dmean','OralCavity - Dmean','Parotid_L - Dmean','Parotid_R - Dmean','PharConsInf - Dmean','PharConsMid - Dmean','PharConsSup - Dmean','SpinalCord - D2','SMG_L - Dmean','SMG_R - Dmean','SupLarynx - D5']

patch_boxplot = np.empty((nb_patients,len(struct_labels_box)))
print("patch_boxplot.shape",patch_boxplot.shape)
patch_boxplot[:,:] = np.NaN
full_boxplot = np.empty((nb_patients,len(struct_labels_box)))
full_boxplot[:,:] = np.NaN
for i in range(len(struct_labels_box)):
    assert struct_labels_box[i] in struct_labels, "Struct {} in box plot not present in trainliststruct".format(struct_labels_box[i])
    print("struct",struct_labels[i])
    print("len(dict_gt[struct_labels[i]][metrics[i]])",len(dict_gt[struct_labels[i]][metrics[i]]))
    print("expr2",len([(dict_patch[struct_labels[i]][metrics[i]][j] - dict_gt[struct_labels[i]][metrics[i]][j])/70*100 for j in range(len(dict_gt[struct_labels[i]][metrics[i]]))]))
    patch_boxplot[:len(dict_gt[struct_labels[i]][metrics[i]]),i]=[(dict_patch[struct_labels[i]][metrics[i]][j] - dict_gt[struct_labels[i]][metrics[i]][j])/70*100 for j in range(len(dict_gt[struct_labels[i]][metrics[i]]))]
    full_boxplot[:len(dict_gt[struct_labels[i]][metrics[i]]),i]=[(dict_full[struct_labels[i]][metrics[i]][j] - dict_gt[struct_labels[i]][metrics[i]][j])/70*100 for j in range(len(dict_gt[struct_labels[i]][metrics[i]]))]
colors = ['deepskyblue','deepskyblue','deepskyblue','orange','orange','orange','orange','orange','orange','deepskyblue','orange','orange','deepskyblue']
patch_edge_color = ['lightskyblue','lightskyblue','lightskyblue','orange','orange','orange','orange','orange','orange','lightskyblue','orange','orange','lightskyblue']
full_edge_color = ['lightgreen','lightgreen','lightgreen','orange','orange','orange','orange','orange','orange','lightgreen','orange','orange','lightgreen']
 
# Filter data using np.isnan
mask = ~np.isnan(patch_boxplot)
patch_boxplot = [d[m] for d, m in zip(patch_boxplot.T, mask.T)]
print("patch_boxplot.shape",len(patch_boxplot))
position_patch = np.arange(1.2,2*len(struct_labels_box),2)
print(position_patch)
mask = ~np.isnan(full_boxplot)
full_boxplot = [d[m] for d, m in zip(full_boxplot.T, mask.T)]
print("full_boxplot.shape",len(full_boxplot))
position_full = np.arange(1.8,2*len(struct_labels_box)+1,2)
print(position_full)

plt.figure()

medianprops = dict(linewidth=2.5, color='k')
box_patch = plt.boxplot(patch_boxplot,positions=position_patch,patch_artist=True, whis=[5, 95],sym='', medianprops=medianprops, showmeans=True)
box_full = plt.boxplot(full_boxplot,positions=position_full,patch_artist=True, whis=[5, 95],sym='', medianprops=medianprops, showmeans=True)

ax = plt.gca()
#ax.set_facecolor('#dee1ff')
for patch, color in zip(box_patch['boxes'], patch_edge_color):
    patch.set_facecolor('lightskyblue')
    patch.set_edgecolor(color)
for patch, color in zip(box_full['boxes'], full_edge_color):
    patch.set_facecolor('lightgreen')
    patch.set_edgecolor(color)
# Boxplot PT
plt.title('Error in DVH metrics for OARs with and without patches')
#ax1.set_xticklabels(struct_labels)#,fontsize=15)
plt.xticks(np.arange(1.5,2*len(struct_labels_box_display)+0.5,2),struct_labels_box_display,rotation=45,ha='right')
plt.ylabel('Patch - No Patch [% of prescription for {} high]'.format(target))
plt.subplots_adjust(bottom=0.3)
plt.grid(axis = 'y')
plt.ylim(-15,20)
plt.tight_layout
ax.legend([box_patch["boxes"][0],box_full["boxes"][0]],["With patches","Without patches"])
plt.savefig(os.path.join(figure_folder,"boxplot_patch_oar.pdf"), format='pdf', dpi=1000)
plt.savefig(os.path.join(figure_folder,"boxplot_patch_oar.jpg"), format='jpg', dpi=1000)

#####################
#    Boxplot TV    #
#####################

metrics = ['D95','D99']
target_labels_box = [target+str(int(p*100))+' - '+ m for p in prescription for m in metrics]
patch_boxplot = np.empty((nb_patients,len(target_labels_box)))
patch_boxplot[:,:] = np.NaN
position_patch = np.arange(1.2,2*len(target_labels_box),2)
full_boxplot = np.empty((nb_patients,len(target_labels_box)))
full_boxplot[:,:] = np.NaN
position_full = np.arange(1.8,2*len(target_labels_box)+1,2)
print("patch_boxplot.shape",patch_boxplot.shape)
print("full_boxplot.shape",full_boxplot.shape)
print(target_labels_box)
for i in range(len(target_labels)):
    for k in range(len(metrics)):
        print("struct",target_labels[i],'metrics',metrics[k])
        print("len(dict_gt[struct_labels[i]][metrics[i]])",len(dict_gt[target_labels[i]][metrics[k]]))
        print("expr2",len([(dict_patch[target_labels[i]][metrics[k]][j] - dict_gt[target_labels[i]][metrics[k]][j])/70*100 for j in range(len(dict_gt[target_labels[i]][metrics[k]]))]))
        
        patch_boxplot[:len(dict_gt[target_labels[i]][metrics[k]]),i*len(target_labels)+k]=[(dict_patch[target_labels[i]][metrics[k]][j] - dict_gt[target_labels[i]][metrics[k]][j])/70*100 for j in range(len(dict_gt[target_labels[i]][metrics[k]]))]
        full_boxplot[:len(dict_gt[target_labels[i]][metrics[k]]),i*len(target_labels)+k]=[(dict_full[target_labels[i]][metrics[k]][j] - dict_gt[target_labels[i]][metrics[k]][j])/70*100 for j in range(len(dict_gt[target_labels[i]][metrics[k]]))]
colors = ['deepskyblue']*len(target_labels_box)
print(colors)

# Filter data using np.isnan
mask = ~np.isnan(patch_boxplot)
patch_boxplot = [d[m] for d, m in zip(patch_boxplot.T, mask.T)]
mask = ~np.isnan(full_boxplot)
full_boxplot = [d[m] for d, m in zip(full_boxplot.T, mask.T)]

plt.figure()

medianprops = dict(linewidth=2.5, color='k')
box_patch = plt.boxplot(patch_boxplot,positions=position_patch,patch_artist=True, whis=[5, 95],sym='', medianprops=medianprops, showmeans=True)
box_full = plt.boxplot(full_boxplot,positions=position_full,patch_artist=True, whis=[5, 95],sym='', medianprops=medianprops, showmeans=True)
ax = plt.gca()
#ax.set_facecolor('#dee1ff')
for patch, color in zip(box_patch['boxes'], colors):
    patch.set_facecolor('lightskyblue')
for patch, color in zip(box_full['boxes'], colors):
    patch.set_facecolor('lightgreen')
# Boxplot PT
plt.title('Error in DVH metrics for TVs with and without patches')
#ax1.set_xticklabels(struct_labels)#,fontsize=15)
plt.xticks(np.arange(1.5,2*len(target_labels_box)+0.5,2),target_labels_box,rotation=45,ha='right')#'vertical')
plt.ylabel('Patch - No Patch [% of prescription for {} high]'.format(target))
plt.subplots_adjust(bottom=0.3)
plt.grid(axis = 'y')
ax.legend([box_patch["boxes"][0],box_full["boxes"][0]],["With patches","Without patches"])
plt.ylim(-15,20)
plt.tight_layout
plt.savefig(os.path.join(figure_folder,"boxplot_patch_tv.pdf"), format='pdf', dpi=1000)
plt.savefig(os.path.join(figure_folder,"boxplot_patch_tv.jpg"), format='jpg', dpi=1000)


# Display slices image patch, image full, difference with oar represented