import os, sys
import numpy as np
import nibabel as nib
from dvh_loss_maialab import mse_final
from utils import *
datafolder      =  r'/path/to/folder/with/patient/VMAT/data'
output_folder    =   r'/folder/where/predictions/are/stored' 
innerfile = 'inner_file_name'
nb_models = 11
params = dict(learn_rate =     3e-4,
                loss_function =  mse_final,
                number_of_pool = 4,
                img_rows =       96, 
                img_cols =       96,
                img_slcs =       64,
                dense_enc_dec =  True,
                instance_norm =  True,
                dilate_bottleneck = False
              )
for k in range(nb_models):
    patientlist=np.load(os.path.join(output_folder,params2name(params),"cross_val_k"+str(k+1),"testlist.npy"))
    resultfolder = os.path.join(output_folder,params2name(params),"test_predictions")
    if not os.path.exists(os.path.join(resultfolder,'pred_nii_norm_ctvp')):
            os.makedirs(os.path.join(resultfolder,'pred_nii_norm_ctvp'))
    if not os.path.exists(os.path.join(resultfolder,'true_nii_norm_ctvp')):
        os.makedirs(os.path.join(resultfolder,'true_nii_norm_ctvp'))
    # Recomputation of doses with normalization by the median over ptv/prescription.
    for patient in patientlist:
        print(patient)
        tv = nib.load(os.path.join(datafolder,patient,innerfile,'struct_tv.nii.gz')).get_fdata().astype(np.int32)
        tv = np.where(tv==70,70,0)
        if not os.path.exists(os.path.join(resultfolder,'pred_nii_norm_ctvp',patient)):
            os.makedirs(os.path.join(resultfolder,'pred_nii_norm_ctvp',patient))
        if not os.path.exists(os.path.join(resultfolder,'true_nii_norm_ctvp',patient)):
            os.makedirs(os.path.join(resultfolder,'true_nii_norm_ctvp',patient))
        Dpre               = tv.max()
        tv                 = tv.astype(bool)
        #Scale the prediction and real dose by the ratio between the mean dose in the PTV and the prescription
        struct = nib.load(os.path.join(datafolder,patient,innerfile,'struct_oar.nii.gz')).get_fdata().astype(np.int32)
        body = np.zeros(struct.shape)
        body[struct!=0]=1.
        
        y_pred = nib.load(os.path.join(resultfolder,'pred_nii', patient,'dose.nii.gz')).get_fdata()
        scaleFactor = np.median(y_pred[tv])/Dpre
        y_pred = y_pred/scaleFactor   

        y_true = nib.load(os.path.join(datafolder,patient,innerfile,'dose.nii.gz')).get_fdata()
        scaleFactor = np.median(y_true[tv])/Dpre
        y_true = y_true/scaleFactor   
        y_pred= np.multiply(y_pred,body)
        y_true = np.multiply(y_true,body)
        nib.save(y_pred,os.path.join(resultfolder,'pred_nii_norm_ctvp', patient,'dose.nii.gz'))
        
        nib.save(y_true,os.path.join(resultfolder,'true_nii_norm_ctvp', patient,'dose.nii.gz'))

