import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras import Input
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow_addons.layers import InstanceNormalization
from tensorflow.keras.layers import concatenate, BatchNormalization, Dropout, Add, Concatenate,PReLU
from tensorflow.keras.layers import Conv3D, UpSampling3D, MaxPool3D, AveragePooling3D
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.initializers import GlorotUniform

from losses import *
from utils import *


'''
Model inspired from Dan Nguyen and SuperPod Team's work - AAPM Dose Prediction Challenge
Each part of the model is implemented in a block.
Instance Normalization is applied instead of Batch Normalization if instance_norm_active = True.

At each stage of the U-shaped network, the user can choose between two blocks:
 - Encoding: DenseConvBlock (dense connectivity) or ConvBlock (non-dense - SuperPod)
 - Bottleneck: DilateBottleNeckConvBlock (dilation rate and dense connectivity - SuperPod) or BottleNeckConvBlock (dense connectivity only)
 - Decoding: DenseUpConvBlock (dense connectivity) or UpConvBlock (non-dense - SuperPod)

'''

class DenseConvBlock(layers.Layer):
    def __init__(self, layer_idx, filter_number, kernel_size, dropout_rate, padding, **kwargs):
        super(DenseConvBlock,self).__init__(**kwargs)
        self.layer_idx = layer_idx
        self.filter_number = filter_number
        self.kernel_size = kernel_size
        self.dropout_rate = dropout_rate
        self.padding = padding

        self.conv3d_1 =  Conv3D(filters=filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu')
        self.bn_1 = BatchNormalization()
        self.in_1 = InstanceNormalization()
        self.do_1 = Dropout(rate = dropout_rate)

        self.conv3d_2 = Conv3D(filters=filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu',name='DenseConvBlock_'+str(layer_idx))
        self.bn_2 = BatchNormalization()
        self.in_2 = InstanceNormalization()
        self.do_2 = Dropout(rate = dropout_rate)
    
    def call(self, inputs, instance_norm_active=True, dropout_active=True, drop_rate_layer=0, **kwargs):
        c1 =self.conv3d_1(inputs)
        if instance_norm_active: n1 = self.in_1(c1)
        else: n1 = self.bn_1(c1)
        if dropout_active : buff1 = self.do_1(n1,training=True)
        else : buff1 = n1
        buff2 = concatenate([buff1,inputs],axis=-1)

        c2 = self.conv3d_2(buff2)
        if instance_norm_active: n2 = self.in_2(c2)
        else: n2 = self.bn_2(c2)
        if dropout_active : buff3 = self.do_2(n2,training=True)
        else : buff3 = n2
        x = concatenate([buff3,buff2], axis=-1)

        return x

    def get_config(self):
        return dict(layer_idx=self.layer_idx,
                    filter_number=self.filter_number,
                    kernel_size=self.kernel_size,
                    dropout_rate=self.dropout_rate,
                    padding=self.padding,
                    **super(DenseConvBlock, self).get_config(),)

class ConvBlock(layers.Layer):
    def __init__(self, layer_idx, filter_number, kernel_size, dropout_rate, padding, **kwargs):
        super(ConvBlock,self).__init__(**kwargs)
        self.layer_idx = layer_idx
        self.filter_number = filter_number
        self.kernel_size = kernel_size
        self.dropout_rate = dropout_rate
        self.padding = padding

        self.conv3d_1 =  Conv3D(filters=filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu')
        self.bn_1 = BatchNormalization()
        self.in_1 = InstanceNormalization()
        self.do_1 = Dropout(rate = dropout_rate)

        self.conv3d_2 = Conv3D(filters=filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu')
        self.bn_2 = BatchNormalization()
        self.in_2 = InstanceNormalization()
        self.do_2 = Dropout(rate = dropout_rate)
    
    def call(self, inputs, instance_norm_active=True, dropout_active=True, drop_rate_layer=0, **kwargs):
        c1 =self.conv3d_1(inputs)
        if instance_norm_active: n1 = self.in_1(c1)
        else: n1 = self.bn_1(c1)
        if dropout_active : buff1 = self.do_1(n1,training=True)
        else : buff1 = n1
        
        c2 = self.conv3d_2(buff1)
        if instance_norm_active: n2 = self.in_2(c2)
        else: n2 = self.bn_2(c2)
        if dropout_active : x = self.do_2(n2,training=True)
        else : x = n2
        
        return x

    def get_config(self):
        return dict(layer_idx=self.layer_idx,
                    filter_number=self.filter_number,
                    kernel_size=self.kernel_size,
                    dropout_rate=self.dropout_rate,
                    padding=self.padding,
                    **super(ConvBlock, self).get_config(),)

class DilateBottleNeckConvBlock(layers.Layer):
    def __init__(self, layer_idx, filter_number, kernel_size, dropout_rate, padding, dilation_rate, **kwargs):
        super(DilateBottleNeckConvBlock,self).__init__(**kwargs)
        self.layer_idx= layer_idx
        self.filter_number = filter_number
        self.kernel_size=kernel_size
        self.dropout_rate=dropout_rate
        self.padding = padding
        self.dilation_rate = dilation_rate

        self.conv3d_1 = Conv3D(filters=filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu', dilation_rate=dilation_rate)
        self.bn_1 = BatchNormalization()
        self.in_1 = InstanceNormalization()
        self.do_1 = Dropout(rate=dropout_rate)

    def call(self, inputs, instance_norm_active=True, dropout_active=True):
        c1 = self.conv3d_1(inputs)
        if instance_norm_active: n1 = self.in_1(c1)
        else: n1 = self.bn_1(c1)
        if dropout_active : buff1 = self.do_1(n1,training=True)
        else : buff1 = n1

        return buff1

    def get_config(self):
        return dict(layer_idx=self.layer_idx,
                    filter_number=self.filter_number,
                    kernel_size=self.kernel_size,
                    dropout_rate=self.dropout_rate,
                    padding=self.padding,
                    dilation_rate=self.dilation_rate,
                    **super(DilateBottleNeckConvBlock, self).get_config(),)

class BottleNeckConvBlock(layers.Layer):
    def __init__(self, layer_idx, filter_number, kernel_size, dropout_rate, padding, **kwargs):
        super(BottleNeckConvBlock,self).__init__(**kwargs)
        self.layer_idx= layer_idx
        self.filter_number = filter_number
        self.kernel_size=kernel_size
        self.dropout_rate=dropout_rate
        self.padding = padding

        self.conv3d_1 = Conv3D(filters=filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu')
        self.bn_1 = BatchNormalization()
        self.in_1 = InstanceNormalization()
        self.do_1 = Dropout(rate=dropout_rate)

    def call(self, inputs, instance_norm_active=True, dropout_active=True):
        c1 = self.conv3d_1(inputs)
        if instance_norm_active: n1 = self.in_1(c1)
        else: n1 = self.bn_1(c1)
        if dropout_active : buff1 = self.do_1(n1,training=True)
        else : buff1 = n1
        return buff1

    def get_config(self):
        return dict(layer_idx=self.layer_idx,
                    filter_number=self.filter_number,
                    kernel_size=self.kernel_size,
                    dropout_rate=self.dropout_rate,
                    padding=self.padding,
                    **super(BottleNeckConvBlock, self).get_config(),)

class UpConvBlock(layers.Layer):
    def __init__(self,layer_idx, filter_number, kernel_size, dropout_rate, pool_size, padding, **kwargs):
        super(UpConvBlock, self).__init__(**kwargs)
        
        self.layer_idx = layer_idx
        self.filter_number = filter_number
        self.kernel_size = kernel_size
        self.dropout_rate = dropout_rate
        self.pool_size = pool_size
        self.padding = padding
        

        self.up_1 = UpSampling3D(size=pool_size)
        self.conv3d_1 = Conv3D(filters=self.filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu')
        self.bn_1 = BatchNormalization()
        self.in_1 = InstanceNormalization()
        self.do_1 = Dropout(rate=dropout_rate)

        self.conv3d_2 = Conv3D(filters=self.filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu')
        self.bn_2 = BatchNormalization()
        self.in_2 = InstanceNormalization()
        self.do_2 = Dropout(rate=dropout_rate)

        self.conv3d_3 = Conv3D(filters=self.filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu')
        self.bn_3 = BatchNormalization()
        self.in_3 = InstanceNormalization()
        self.do_3 = Dropout(rate=dropout_rate)

    def call(self, inputs, skip_input, instance_norm_active=True, dropout_active = True, **kwargs):
        u1 = self.up_1(inputs)
        c1 = self.conv3d_1(u1)
        if instance_norm_active: n1 = self.in_1(c1)
        else: n1 = self.bn_1(c1)
        if dropout_active : buff1 = self.do_1(n1,training=True)
        else : buff1 = n1
        buff2 = concatenate([buff1, skip_input],axis=-1)
        
        c2 = self.conv3d_2(buff2)
        if instance_norm_active: n2 = self.in_2(c2)
        else: n2 = self.bn_2(c2)
        if dropout_active : buff3 = self.do_2(n2,training=True)
        else : buff3 = n2
        
        c3 = self.conv3d_3(buff3)
        if instance_norm_active: n3 = self.in_3(c3)
        else: n3 = self.bn_3(c3)
        if dropout_active : x = self.do_3(n3,training=True)
        else : x = n3
        return x

    def get_config(self):
        return dict(pool_size=self.pool_size,
                    layer_idx=self.layer_idx,
                    filter_number=self.filter_number,
                    kernel_size=self.kernel_size,
                    dropout_rate=self.dropout_rate,
                    padding=self.padding,
                    **super(UpConvBlock, self).get_config(),)


class DenseUpConvBlock(layers.Layer):
    def __init__(self,layer_idx, filter_number, kernel_size, dropout_rate, pool_size, padding, **kwargs):
        super(DenseUpConvBlock, self).__init__(**kwargs)
        self.layer_idx = layer_idx
        self.filter_number = filter_number
        self.kernel_size = kernel_size
        self.dropout_rate = dropout_rate
        self.pool_size = pool_size
        self.padding = padding

        self.up_1 = UpSampling3D(size=pool_size)
        self.conv3d_1 = Conv3D(filters=self.filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu')
        self.bn_1 = BatchNormalization()
        self.in_1 = InstanceNormalization()
        self.do_1 = Dropout(rate=dropout_rate)

        self.conv3d_2 = Conv3D(filters=self.filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu')
        self.bn_2 = BatchNormalization()
        self.in_2 = InstanceNormalization()
        self.do_2 = Dropout(rate=dropout_rate)

        self.conv3d_3 = Conv3D(filters=self.filter_number, kernel_size=kernel_size, padding='same',kernel_initializer=GlorotUniform(),activation='relu')
        self.bn_3 = BatchNormalization()
        self.in_3 = InstanceNormalization()
        self.do_3 = Dropout(rate=dropout_rate)

    def call(self, inputs, skip_input, instance_norm_active=True, dropout_active = True, **kwargs):
        u1 = self.up_1(inputs)
        c1 = self.conv3d_1(u1)
        if instance_norm_active: n1 = self.in_1(c1)
        else: n1 = self.bn_1(c1)
        if dropout_active : buff1 = self.do_1(n1,training=True)
        else : buff1 = n1
        buff2 = concatenate([buff1, skip_input],axis=-1)

        c2 = self.conv3d_2(buff2)
        if instance_norm_active: n2 = self.in_2(c2)
        else: n2 = self.bn_2(c2)
        if dropout_active : buff3 = self.do_2(n2,training=True)
        else : buff3 = n2
        buff4 = concatenate([buff3, buff2],axis=-1)

        c3 = self.conv3d_3(buff4)
        if instance_norm_active: n3 = self.in_3(c3)
        else: n3 = self.bn_3(c3)
        if dropout_active : buff5 = self.do_3(n3,training=True)
        else : buff5 = n3
        x = concatenate([buff5, buff4],axis=-1)

        return x

    def get_config(self):
        return dict(layer_idx=self.layer_idx,
                    filter_number=self.filter_number,
                    kernel_size=self.kernel_size,
                    dropout_rate=self.dropout_rate,
                    pool_size=self.pool_size,
                    padding=self.padding,
                    **super(DenseUpConvBlock, self).get_config(),)


def build_3d_unet(img_rows=None, img_cols=None, img_slcs=None, channels_in=None, channels_out=None, filter_number=16, kernel_size=(3,3,3), number_of_pool=4, pool_size=(2,2,2), dropout_rate=0, expansion_rate=1, compression=0.5, final_activation='relu', loss_function=None, learn_rate=1e-3, decay_rate=0, dense_enc_dec=False, instance_norm =  True, dilate_bottleneck = True):
    if compression >= 1:
        print('Cannot have compression greater than or equal to one. Setting compression to default value of 0.5')
        compression = 0.5
    elif compression <= 0:
        print('Cannot have compression less than or equal to zero. Setting compression to default value of 0.5')
        compression = 0.5

    initializer=GlorotUniform
    layer_conv={}
    layer_nonconv={}
    buffconvpool={}
    bottom_layers = {}

    number_of_layers_half=number_of_pool+1
    conv_params = dict( kernel_size=kernel_size,
                        dropout_rate = dropout_rate,
                        padding = 'same')    

    """
        Encoding Part
    """
    number_of_filters_max = np.round((expansion_rate**(number_of_layers_half-1))*filter_number)
    layer_nonconv[0] = Input((img_rows, img_cols, img_slcs, channels_in))
    
    for layer_number in range(1,number_of_layers_half):
        number_of_filters_current = np.round((expansion_rate**(layer_number-1))*filter_number)
        
        drop_rate_layer = dropout_rate * (np.sqrt((number_of_filters_current/number_of_filters_max)))
        if dense_enc_dec:
            layer_conv[layer_number] = DenseConvBlock(layer_idx=layer_number, filter_number = number_of_filters_current, **conv_params)(layer_nonconv[layer_number-1])        
            buffconvpool[layer_number] = BatchNormalization()(Conv3D(filters=number_of_filters_current, kernel_size=pool_size, strides=pool_size,padding='same',kernel_initializer=initializer(),activation='relu')(layer_conv[layer_number]))
            buffmaxpool = MaxPool3D(pool_size=pool_size)(layer_conv[layer_number])
            layer_nonconv[layer_number] = concatenate([buffconvpool[layer_number],buffmaxpool], axis=-1)
        else:
            layer_conv[layer_number] = ConvBlock(layer_idx=layer_number, filter_number = number_of_filters_current, **conv_params)(layer_nonconv[layer_number-1])
            buffmaxpool = MaxPool3D(pool_size=pool_size)(layer_conv[layer_number])
            layer_nonconv[layer_number] = buffmaxpool
    number_of_filters_current = expansion_rate**(number_of_pool)*filter_number
    if dilate_bottleneck:
        """
            DilateBottleneck, same color code as in the presentation of SuperPod
        """
        x = DilateBottleNeckConvBlock(layer_idx=number_of_pool, dilation_rate = 1, filter_number=number_of_filters_current, **conv_params)(layer_nonconv[number_of_layers_half-1])
        bottom_layers[0] = x # bleu
        x = DilateBottleNeckConvBlock(layer_idx=number_of_pool, dilation_rate = 2, filter_number=number_of_filters_current, **conv_params)(x)
        bottom_layers[1] = x # rose pale
        x = DilateBottleNeckConvBlock(layer_idx=number_of_pool, dilation_rate = 5, filter_number=number_of_filters_current, **conv_params)(concatenate([x,bottom_layers[0]]))
        bottom_layers[2] = x # rose fonce
        x = DilateBottleNeckConvBlock(layer_idx=number_of_pool, dilation_rate = 9, filter_number=number_of_filters_current, **conv_params)(concatenate([x,bottom_layers[0],bottom_layers[1]]))
        bottom_layers[3] = x # gris fonce
        x = DilateBottleNeckConvBlock(layer_idx=number_of_pool, dilation_rate = 1, filter_number=number_of_filters_current, **conv_params)(concatenate([x,bottom_layers[0],bottom_layers[1],bottom_layers[2]]))
        bottom_layers[4] = x # vert clair
        x = DilateBottleNeckConvBlock(layer_idx=number_of_pool, dilation_rate = 2, filter_number=number_of_filters_current, **conv_params)(concatenate([x,bottom_layers[0],bottom_layers[1],bottom_layers[2],bottom_layers[3]]))
        bottom_layers[5] = x # rouge
        x = DilateBottleNeckConvBlock(layer_idx=number_of_pool, dilation_rate = 5, filter_number=number_of_filters_current, **conv_params)(concatenate([x,bottom_layers[0],bottom_layers[1],bottom_layers[2],bottom_layers[3],bottom_layers[4]]))
        bottom_layers[6] = x # violet    
        x = DilateBottleNeckConvBlock(layer_idx=number_of_pool, dilation_rate = 9, filter_number=number_of_filters_current, **conv_params)(concatenate([x,bottom_layers[0],bottom_layers[1],bottom_layers[2],bottom_layers[3],bottom_layers[4],bottom_layers[5]]))
        bottom_layers[7] = x # violet apres conv
        
        layer_conv[number_of_layers_half] = concatenate([bottom_layers[0],bottom_layers[1],bottom_layers[2],bottom_layers[3],bottom_layers[4],bottom_layers[5], bottom_layers[6], bottom_layers[7]])
    else:    
        """
            Original Bottleneck
        """
        buff1 = BottleNeckConvBlock(layer_idx=number_of_pool, filter_number=number_of_filters_current, **conv_params)(layer_nonconv[number_of_layers_half-1])
        buff2 = concatenate([buff1,layer_nonconv[number_of_layers_half-1]],axis=-1)
        buff3 = BottleNeckConvBlock(layer_idx=number_of_pool, filter_number=number_of_filters_current, **conv_params)(buff2)
        buff4 = concatenate([buff3,buff2], axis=-1)
        buff5 = BottleNeckConvBlock(layer_idx=number_of_pool, filter_number=number_of_filters_current, **conv_params)(buff4)
        buff6 = concatenate([buff5, buff4], axis=-1)
        buff7 = BottleNeckConvBlock(layer_idx=number_of_pool, filter_number=number_of_filters_current, **conv_params)(buff6)
        layer_conv[number_of_layers_half] = concatenate([buff7,buff6], axis=-1)
    """
        Decoding Part
    """
    for i,layer_number in enumerate(range(number_of_layers_half+1,2*number_of_layers_half)):
        number_of_filters_current = np.round((expansion_rate**(2*number_of_layers_half-layer_number-1))*filter_number)
        if dense_enc_dec:
            layer_conv[layer_number] = DenseUpConvBlock(layer_idx=i, pool_size=pool_size, filter_number=number_of_filters_current, **conv_params)(layer_conv[layer_number-1], layer_conv[2*number_of_layers_half-layer_number])#concatenate([buff3,buff2], axis=-1)
        else:
            layer_conv[layer_number] = UpConvBlock(layer_idx=i, pool_size=pool_size, filter_number=number_of_filters_current, **conv_params)(layer_conv[layer_number-1], layer_conv[2*number_of_layers_half-layer_number])#concatenate([buff3,buff2], axis=-1)
        

    layer_conv[2 * number_of_layers_half] = Conv3D(filters=channels_out, kernel_size=kernel_size, padding='same',kernel_initializer=initializer(),activation=final_activation)(layer_conv[2 * number_of_layers_half - 1])

    model = Model(inputs=[layer_nonconv[0]], outputs=[layer_conv[2 * number_of_layers_half]])
    model.compile(optimizer=Adam(lr=learn_rate,decay=decay_rate), loss=loss_function())

    return model
