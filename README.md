# NTCP_predicted_dose

## Project
This repository accompagnies the article "Patient selection for proton therapy using Normal Tissue Complication Probability estimation after fast dose prediction with deep learning for oropharyngeal head and neck cancer".

## Data format
We used the Nifti format to contain the data used in the training of the models in the following manner:
The nifti headers all contain information common to all files such as the voxelsize (pixdim) and arrayshape (dim).
- ct.nii.gz: contains the CT grid.
- struct_oar.nii.gz: contains the masks of organs at risks, one hot encoded. The header of the nifti contains an additional extension field containing the contours_exist list (1 if the corresponding OAR is present, 0 otherwise).
- struct_tv.nii.gz: contains the target volumes masks with prescription value encoded in the voxels.
- prior_knowledge.nii.gz: contains the dose distribution when applying the template in the treatment planning software without the manual tuning of the dosimetrists. 
- dose.nii.gz: contains the clinical dose distribution, the label for the training.

## Preprocessing
Preprocessing steps consisted of resampling all matrices (CT, doses, structures) to a voxel size of 3mmx3mmx3mm. 
Post processing specific to STRUCT:
- Selection of the following structures (which names were cleaned beforehand): ['BODY', 'BRAINSTEM', 'ESOPHAGUS_UPPER', 'GLOTTICAREA',
       'ORALCAVITY', 'PAROTID_L', 'PAROTID_R', 'PHARCONSINF',
       'PHARCONSMID', 'PHARCONSSUP', 'SMG_L', 'SMG_R', 'SPINALCORD',
       'SUPRAGLOTLARYNX', 'CTV_5425', 'CTV_7000']
Post processing specific to CT:
- Clipping of HU values under -1000 and over 1560. 
- Setting the voxels outside the body contour to -1000 to remove the couch.

## Architecture